#D----------------------------------------------------------------
rm(list = ls ())
library(colorspace)
library(chemCal)

cur_dir <- getwd()
if (!grepl("/SPP", cur_dir, ignore.case = TRUE)) {
    setwd("./SPP")
}

sol0 <- read.table("data/TB_H2O_Absorbance_01.txt", header = F, skip =16)
sol1 <- read.table("data/TB_Absorbance_01.txt", header = F, skip =16)
sol2 <- read.table("data/TB_Absorbance_02.txt", header = F, skip =16)
sol3 <- read.table("data/TB_Absorbance_03.txt", header = F, skip =16)
sol4 <- read.table("data/TB_Absorbance_04.txt", header = F, skip =16)
sol5 <- read.table("data/TB_Absorbance_05.txt", header = F, skip =16)
sol6 <- read.table("data/TB_Absorbance_06.txt", header = F, skip =16)
sol7 <- read.table("data/TB_Absorbance_07.txt", header = F, skip =16)
sol8 <- read.table("data/TB_Absorbance_08.txt", header = F, skip =16)
sol9 <- read.table("data/TB_TaskE_Absorbance_1.txt", header = F, skip =16)
sol10 <-read.table("data/TB_HCl_Absorbance_01.txt", header = F, skip = 16)

density_h2o <- 0.997
density_hcl <- 1.02
m_A <- c(14.915, 9.924, 9.918, 9.874, 9.687, 8.786, 7.508, 5.0032, 9.787)
m_B <- c(0.0015, 0.025, 0.081, 0.1570, 0.3195, 1.261, 2.521, 5.032, 0.221)
n_H <- m_B / density_hcl*1
c_H <- n_H /(m_B/ density_hcl+m_A/ density_h2o)
pH <- -log10(c_H)
idx <- 1775

A <- c(sol1[idx, 2], sol2[idx ,2] , sol3[idx ,2] , sol4[idx ,2] , sol5[idx ,2] , sol6[idx ,2] , sol7[
idx ,2] , sol8[idx ,2], sol9[idx, 2])

# the model
y_model <- log10 ((A - sol10[idx ,2]) / ( sol0[idx ,2] - A))
model <- lm(y_model[2:9] ~ pH[2:9])
pKa <- inverse.predict(model , 0)
cat(" pKa = ", pKa$Prediction , "+-", pKa$Confidence , "\n")


# plotting all spectra
if (1) {
windows(height =7, width =9)
par(mar= c(5, 6, 3, 2) + 0.1)

col <- sequential_hcl(11, palette = "Viridis")

plot (NA , las =1, ann =F, xlim = c(180 , 900) ,ylim = c(0 ,2.4) )

lines(sol0, lwd = 2, col = col[11])
lines(sol1, lwd = 2, col = col[7])
lines(sol2, lwd = 2, col = col[6])
lines(sol3, lwd = 2, col = col[8])
lines(sol4, lwd = 2, col = col[9])
lines(sol5, lwd = 2, col = col[5])
lines(sol6, lwd = 2, col = col[4])
lines(sol7, lwd = 2, col = col[3])
lines(sol8, lwd = 2, col = col[2])
lines(sol9, lwd = 2, col = col[1])
lines(sol10, lwd = 2, col = col[10])

points(sol0[idx ,1], sol0[idx ,2], pch =21, bg= "white")
points(sol1[idx ,1], sol1[idx ,2], pch =4, col = "red")
points(sol2[idx ,1], sol2[idx ,2], pch =21, bg= "white")
points(sol3[idx ,1], sol3[idx ,2], pch =21, bg= "white")
points(sol4[idx ,1], sol4[idx ,2], pch =21, bg= "white")
points(sol5[idx ,1], sol5[idx ,2], pch =21, bg= "white")
points(sol6[idx ,1], sol6[idx ,2], pch =21, bg= "white")
points(sol7[idx ,1], sol7[idx ,2], pch =21, bg= "white")
points(sol8[idx ,1], sol8[idx ,2], pch =21, bg= "white")
points(sol9[idx ,1], sol9[idx ,2], pch =21, bg= "white")
points(sol10[idx ,1], sol10[idx ,2], pch =21, bg= "white")

mtext ( side = 1, text = expression (lambda ~ "/ nm"), line =3, cex =1.5)
mtext ( side = 2, text = "Absorbance", line = 4, cex = 1.5)

axis (1, seq(0, 1000, by =50), lab = NA, tck = -0.01)
axis (2, seq(-2, 10, by =0.1), lab = NA, tck = -0.01)

legend("topright", c("pH = 7", "pH = 0"), col =c(col[11] , col[10]) ,lty=c
(1 ,1) ,lwd =c(2 ,2))
# points <- locator (n=3)
dev.copy2pdf(file ="plots/D_spectra.pdf")
dev.off()
}

# plotting the linear model
if (1) {
windows(height =7, width =9)
par(mar = c(5, 6, 3, 2) + 0.1)

plot (NA,
    ann =F,
    las =1,
    xlim = c(0 ,5),
    ylim =c( -1.6 , 2)
)

abline(model ,lwd =1.5)
arrows(-1,0, pKa $ Prediction ,0, lty =2, length =0, lwd =1.5)
arrows(pKa$Prediction, -2, pKa$Prediction, 0, lty = 2, length = 0, lwd = 1.5)
points(pH[2:9] ,y_model[2:9] ,pch = 21 , bg = "white",lwd = 1.5)
points(pH[1], y_model[1], pch = 4, col = "red",cex = 1.1)

mtext(side = 1, text = "pH", line =3, cex =1.5)
mtext(side = 2, text = expression(log(frac(italic(A) - italic(A)[HB]^0 , italic(A)[B]^0 - italic (A)))), line =2.1 , cex =1.5)

axis (1, seq (0, 10, by =0.25) , lab = NA , tck = -0.01)
axis (2, seq (-2, 10, by =0.1) , lab = NA , tck = -0.01)

dev.copy2pdf(file ="plots/D_pH.pdf")
dev.off ()
}