import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import pandas as pd

df = pd.read_csv('TRF/data/day_two/0_TRPL_EXC_355nm_20MHz_Slit_0.1%_EM_480nm_SLit_27nm_Att_100_100s/Custom_20231128_1552_0.csv',delimiter='\t')
print(df)
# plt.plot(df['Time'],df['Intensity'])
#plt.show()

# Define the fitting function
def fitting_function(x, a, b, c, d):
    return a * np.exp(b * x) + c * np.exp(d * x)


max =df['Intensity'].idxmax()
# Skip the first 40 rows
x_data = df['Time'][max:]
y_data = df['Intensity'][max:]


initial_guess = (500000, -1.3, -0.3, -3.4)  # Initial guess for parameters (a, b, c, d)
fit_params, covariance = curve_fit(fitting_function, x_data, y_data, p0=initial_guess)



# # Display the fitted parameters
print(f"Fitted Parameters: a={fit_params[0]}, b={fit_params[1]}, c={fit_params[2]}, d={fit_params[3]}")

# # Plot the results
plt.scatter(x_data, y_data, label='Data')
plt.plot(x_data, fitting_function(x_data, *fit_params), 'r-', label='Fitted Curve')
plt.title('Curve Fitting Example')
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.show()

# a=492275.1945725465, b=-0.5795456133987015, c=-0.29903687442509025, d=-4890.489201021719 