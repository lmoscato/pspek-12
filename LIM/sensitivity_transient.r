rm(list = ls()) # Usual stuff.

# packages ---------------------------------------------
library("colorspace")
library("extrafont")
library("magicaxis")
library("errors")
library("httpgd")


# other rules -------------------------------------------

# This if statment verifies if the working directory is set correctly, if it's not, it sets
# the correct one. (pspek-12/LIM)
cur_dir <- getwd()
if (!grepl("/LIM", cur_dir, ignore.case = TRUE)) {
    setwd("./LIM")
}
source("../minorticks.r")
source("find_error.r")
rm(cur_dir)
options(errors.notation = "plus-minus", errors.digits = 2) # write errors as plus minus after value

par(family = "CM Roman") # LaTeX font

# data reading ------------------------------------------------------------------
trans4 <- read.csv(skip = 3, sep = ",", file = "data/TRANS4.D/TRANS4.d#/TRANS4.csv") #read data from file
trans10 <- read.csv(skip = 3, sep = ",", file = "data/TRANS10.D/TRANS10.d#/TRANS10.csv")
trans50 <- read.csv(skip = 3, sep = ",", file = "data/TRANS50.D/TRANS50.d#/TRANS50.csv")

# big mess with errors and names follows, yay
lit_data <- read.csv(sep = ",", file = "data/nist610data.csv", header = TRUE)

lit_concentrations <- lit_data[, 2]
names(lit_concentrations) <- lit_data[, 1]

uncertainties <- lit_data[, 3]
names(uncertainties) <- lit_data[, 1]

elements_measured <- c("Li", "Na2O", "SiO2", "CaO", "Ti", "Ti", "Mn", "Fe", "Fe", "Ni", "Zn", "Sr", "Mo", "Cs", "Pb", "Th", "U")

lit_concentrations2 <- lit_concentrations[elements_measured]
lit_concentrations <- lit_data[, 3]

isotopic_abundances <- 1e-2 * c(92.41, 100, 4.683, 2.09, 7.44, # isotopic abundances
                                73.72, 100, 91.75, 2.119, 26.223,
                                27.90, 82.58, 24.13, 1, 52.4, 1, 99.27)

errors(lit_concentrations2) <- uncertainties[elements_measured]
names(isotopic_abundances) <- names(trans4[, 2:18])

# conversion in cps------------------------------------------------
for (i in 2:18) {
    trans4[, i] <- trans4[, i] * 250
    # trans4[, i] <- log10(trans4[, i])
    trans10[, i] <- trans10[, i] * 100
    # trans10[, i] <- log10(trans10[, i])
    trans50[, i] <- trans50[, i] * 20
    # trans50[, i] <- log10(trans50[, i])
}

# index hunting -------------------------------------------------
trans4_bg_idx <- which(trans4[,1] > 2 & trans4[,1] < 37) # finding indeces for backgorund
trans10_bg_idx <- which(trans10[,1] > 2 & trans10[,1] < 37)
trans50_bg_idx <- which(trans50[,1] > 2 & trans50[,1] < 37)

trans4_meas_idx <- which(trans4[,1] > 65 & trans4[,1] < 120) #finding indeces for measurement
trans10_meas_idx <- which(trans10[,1] > 65 & trans10[,1] < 120) 
trans50_meas_idx <- which(trans50[,1] > 65 & trans50[,1] < 120)


palette <- rainbow_hcl(17) # chromatic palette

# plotting transient 4 ms --------------------------------------------
# we neeed just one of these, the others we don't plot, we just calculate
plot(trans4[, 1], trans4[, 2], col = palette[1], type = "l",
    ylim = c(1e1, 1e9),
    xlab = "Time [s]",
    log = "y",
    ylab = "Intensity [CPS]",
    las = 1,
    yaxt = "n",
    xaxt = "n",
    cex.lab = 1.3
)
magaxis(1, hersh = TRUE, family = "serif", minorn = 5, cex.axis = 1.3) #pretty axes for plot
magaxis(2, hersh = TRUE, family = "serif", minorn = 9, cex.axis = 1.3)

for (i in c(4,7,10,17)) { # plot everything with for loop
    lines(trans4[, 1], trans4[, i],
        col = palette[i - 1],
        log = "y"
    )
}

legend("topright",legend = names(trans10[,c(2,4,7,10,17)]),
    lty = 1,
    col = palette[c(2,4,7,10,17) - 1],
    cex = 1.4
)

abline(v = c(trans4[min(trans4_bg_idx), 1], trans4[max(trans4_bg_idx), 1]), # lines for background
    col = "royalblue",
    lty = 2,
    lwd = 2.5
)

abline(v = c(trans4[min(trans4_meas_idx), 1], trans4[max(trans4_meas_idx), 1]),
    col = "#4af020",
    lty = 2,
    lwd = 2.5
)





# calculations---------------------------
bg_means4 <- c()
bg_means10 <- c()
bg_means50 <- c()

meas_means4 <- c()
meas_means10 <- c()
meas_means50 <- c()

for (i in 2:18) {
    bg_means4 <- append(bg_means4, mean(trans4[trans4_bg_idx, i]))
    bg_means10 <- append(bg_means10, mean(trans10[trans10_bg_idx, i]))
    bg_means50 <- append(bg_means50, mean(trans50[trans50_bg_idx, i]))

    meas_means4 <- append(meas_means4, mean(trans4[trans4_meas_idx, i]))
    meas_means10 <- append(meas_means10, mean(trans10[trans10_meas_idx, i]))
    meas_means50 <- append(meas_means50, mean(trans50[trans50_meas_idx, i]))
}

names(bg_means4) <- names(trans4[, 2:18])
names(bg_means10) <- names(trans10[, 2:18])
names(bg_means50) <- names(trans50[, 2:18])

names(meas_means4) <- names(trans4[, 2:18])
names(meas_means10) <- names(trans10[, 2:18])
names(meas_means50) <- names(trans50[, 2:18])

cps_4 <- meas_means4 - bg_means4
cps_10 <- meas_means10 - bg_means10
cps_50 <- meas_means50 - bg_means50

sensitivity4 <- cps_4 / (lit_concentrations2 * isotopic_abundances) #vector for sensitivities of 4 ms
sensitivity10 <- cps_10 / (lit_concentrations2 * isotopic_abundances)
sensitivity50 <- cps_50 / (lit_concentrations2 * isotopic_abundances)

output_data <- data.frame(sensitivity4, errors(sensitivity4), sensitivity10, errors(sensitivity10), sensitivity50, errors(sensitivity50))

write.csv(output_data, file = "transient_sensitivity_data.txt", row.names = TRUE, col.names = FALSE)

names(palette) <- names(trans10[,2:18])


# same element diff IT plot-------------------------------------------

plot(trans10[,"Time..Sec."],trans10[,"Ni60"] / 100,
    type = "l",
    col = palette[1],
    xaxt = "n",
    yaxt = "n",
    xlab = "Time [s]",
    ylab = "Intensity [Counts]",
    log = "y",
    ylim = c(1,1e4),
    cex.lab = 1.3
)

magaxis(1, hersh = TRUE, family = "serif", minorn = 5, cex.axis = 1.3)
magaxis(2, hersh = TRUE, family = "serif", minorn = 9, cex.axis = 1.3)

lines(trans4[,1], trans4[,"Ni60"] / 250,
    col = palette[5]
)

lines(trans50[,1], trans50[, "Ni60"] / 20,
    col = palette[10]
)

legend("topright",
    legend = c("4 ms", "10 ms", "50 ms"),
    col = palette[c(5,1,10)],
    lty = 1,
    cex = 1.4
)