remove(list = ls()) # Usual stuff.

# packages ---------------------------------------------warnin
library("colorspace")
library("extrafont")
library("magicaxis")
library("errors")
library("httpgd")

# other rules -------------------------------------------

# This if statment verifies if the working directory is set correctly, if it's not, it sets
# the correct one. (pspek-12/LIM)
cur_dir <- getwd()
if (!grepl("/LIM", cur_dir, ignore.case = TRUE)) {
    setwd("./LIM")
}
source("../minorticks.r")
source("find_error.r")
rm(cur_dir)
options(errors.notation = "plus-minus", errors.digits = 2) # write errors as plus minus after value

par(family = "CM Roman") # LaTeX font
#x11(width = 8, height = 6) #open device

# data reading ------------------------------------------------------------------
trans11 <- read.csv(skip = 3, sep = ",", file = "data/612_1.D/612_1.d#/612_1.csv") #read data from file
trans12 <- read.csv(skip = 3, sep = ",", file = "data/612_2.D/612_2.d#/612_2.csv")
trans21 <- read.csv(skip = 3, sep = ",", file = "data/612_3.D/612_3.d#/612_3.csv")
trans22 <- read.csv(skip = 3, sep = ",", file = "data/612_4.D/612_4.d#/612_4.csv")


lit_data <- read.csv(sep = ",", file = "data/nist610data.csv", header = TRUE)
lit_concentrations <- lit_data[, 2]
names(lit_concentrations) <- lit_data[, 1]
uncertainties <- lit_data[, 3]
names(uncertainties) <- lit_data[, 1]


elements_measured <- c("Li", "Na", "Si", "Ca", "Sc", "Ti", "Mn", "Co", "Ga", "As", "Y", "Nb", "Pd", "Sb", "Cs", "Ho", "Au", "Th", "U")

#Li7,Na23,Si29,Ca44,Sc45,Ti48,Mn55,Co59 100,Ga69 60.108,As75 100,Y89 100,Nb93 100,Pd105 22.33,Sb121 57.21,Cs133 100,Ho165 100,Au197 100,Th232 100,U238 99.2742

lit_concentrations2 <- lit_concentrations[elements_measured]

isotopic_abundances <- 1e-2 * c(92.41, 100, 4.683, 2.09, 100, # isotopic abundances
                                73.72, 100, 100, 60.108,
                                100, 100, 100, 22.33, 57.21, 100,
                                100, 100, 100, 99.2742)

errors(lit_concentrations2) <- uncertainties[elements_measured]
names(isotopic_abundances) <- names(trans11[, 2:20])

# conversion in cps------------------------------------------------
for (i in 2:20) {
    trans11[, i] <- trans11[, i] * 100
    # trans4[, i] <- log10(trans4[, i])
    trans12[, i] <- trans12[, i] * 100
    # trans10[, i] <- log10(trans10[, i])
    trans21[, i] <- trans21[, i] * 100
    # trans50[, i] <- log10(trans50[, i])
    trans22[, i] <- trans22[, i] * 100
}

# index hunting -------------------------------------------------
trans11_bg_idx <- which(trans11[,1] > 2 & trans11[,1] < 37) # finding indeces for backgorund
trans12_bg_idx <- which(trans12[,1] > 2 & trans12[,1] < 37)
trans21_bg_idx <- which(trans21[,1] > 2 & trans21[,1] < 37)
trans22_bg_idx <- which(trans22[,1] > 2 & trans22[,1] < 37)

trans11_meas_idx <- which(trans11[,1] > 65 & trans11[,1] < 120) #finding indeces for measurement
trans12_meas_idx <- which(trans12[,1] > 65 & trans12[,1] < 120) 
trans21_meas_idx <- which(trans21[,1] > 65 & trans21[,1] < 120)
trans22_meas_idx <- which(trans22[,1] > 65 & trans22[,1] < 120)


palette <- rainbow_hcl(19) # chromatic palette

# plotting transient 612 tray 1--------------------------------------------
# we neeed just one of these, the others we don't plot, we just calculate
plot(trans11[, 1], trans11[, 2], col = palette[1], type = "l",
    ylim = c(1e1, 1e9),
    xlab = "t [s]",
    log = "y",
    ylab = "cps",
    las = 1,
    yaxt = "n",
    xaxt = "n"
)
magaxis(1, hersh = TRUE, family = "serif", minorn = 5) #pretty axes for plot
magaxis(2, hersh = TRUE, family = "serif", minorn = 9)

for (i in 3:20) { # plot everything with for loop
    lines(trans11[, 1], trans11[, i],
        col = palette[i - 1],
        log = "y"
    )
}

abline(v = c(trans11[min(trans11_bg_idx), 1], trans11[max(trans11_bg_idx), 1]), # lines for background
    col = "royalblue",
    lty = 2,
    lwd = 2.5
)

abline(v = c(trans11[min(trans11_meas_idx), 1], trans11[max(trans11_meas_idx), 1]),
    col = "purple",
    lty = 2,
    lwd = 2.5
)

# plotting transient 612 tray 2--------------------------------------------
# we neeed just one of these, the others we don't plot, we just calculate
plot(trans12[, 1], trans12[, 2], col = palette[1], type = "l",
    ylim = c(1e1, 1e9),
    xlab = "t [s]",
    log = "y",
    ylab = "cps",
    las = 1,
    yaxt = "n",
    xaxt = "n"
)
magaxis(1, hersh = TRUE, family = "serif", minorn = 5) #pretty axes for plot
magaxis(2, hersh = TRUE, family = "serif", minorn = 9)

for (i in 3:20) { # plot everything with for loop
    lines(trans12[, 1], trans12[, i],
        col = palette[i - 1],
        log = "y"
    )
}

abline(v = c(trans12[min(trans12_bg_idx), 1], trans12[max(trans12_bg_idx), 1]), # lines for background
    col = "royalblue",
    lty = 2,
    lwd = 2.5
)

abline(v = c(trans12[min(trans12_meas_idx), 1], trans12[max(trans12_meas_idx), 1]),
    col = "purple",
    lty = 2,
    lwd = 2.5
)


# plotting transient 612 tray 3--------------------------------------------
# we neeed just one of these, the others we don't plot, we just calculate
plot(trans21[, 1], trans21[, 2], col = palette[1], type = "l",
    ylim = c(1e1, 1e9),
    xlab = "t [s]",
    log = "y",
    ylab = "cps",
    las = 1,
    yaxt = "n",
    xaxt = "n"
)
magaxis(1, hersh = TRUE, family = "serif", minorn = 5) #pretty axes for plot
magaxis(2, hersh = TRUE, family = "serif", minorn = 9)

for (i in 3:20) { # plot everything with for loop
    lines(trans12[, 1], trans21[, i],
        col = palette[i - 1],
        log = "y"
    )
}

abline(v = c(trans21[min(trans11_bg_idx), 1], trans21[max(trans21_bg_idx), 1]), # lines for background
    col = "royalblue",
    lty = 2,
    lwd = 2.5
)

abline(v = c(trans21[min(trans21_meas_idx), 1], trans21[max(trans21_meas_idx), 1]),
    col = "purple",
    lty = 2,
    lwd = 2.5
)

# plotting transient 612 tray 4--------------------------------------------
# we neeed just one of these, the others we don't plot, we just calculate
plot(trans22[, 1], trans22[, 2], col = palette[1], type = "l",
    ylim = c(1e1, 1e9),
    xlab = "t [s]",
    log = "y",
    ylab = "cps",
    las = 1,
    yaxt = "n",
    xaxt = "n"
)
magaxis(1, hersh = TRUE, family = "serif", minorn = 5) #pretty axes for plot
magaxis(2, hersh = TRUE, family = "serif", minorn = 9)

for (i in 3:20) { # plot everything with for loop
    lines(trans22[, 1], trans22[, i],
        col = palette[i - 1],
        log = "y"
    )
}

abline(v = c(trans22[min(trans11_bg_idx), 1], trans22[max(trans11_bg_idx), 1]), # lines for background
    col = "royalblue",
    lty = 2,
    lwd = 2.5
)

abline(v = c(trans22[min(trans11_meas_idx), 1], trans22[max(trans11_meas_idx), 1]),
    col = "purple",
    lty = 2,
    lwd = 2.5
)

#plot Titanium

plot(trans22[,1 ], trans22[, "Ti48"],  type = "l",
    ylim = c(1e1, 1e9),
    xlab = "t [s]",
    log = "y",
    ylab = "cps",
    las = 1,
    yaxt = "n",
    xaxt = "n"
)
magaxis(1, hersh = TRUE, family = "serif", minorn = 5) #pretty axes for plot
magaxis(2, hersh = TRUE, family = "serif", minorn = 9)

lines(trans21[,1 ], trans21[, "Ti48"])
lines(trans11[,1 ], trans11[, "Ti48"])
lines(trans12[,1 ], trans12[, "Ti48"])

abline(v = c(trans22[min(trans11_bg_idx), 1], trans22[max(trans11_bg_idx), 1]), # lines for background
    col = "royalblue",
    lty = 2,
    lwd = 2.5
)

abline(v = c(trans22[min(trans11_meas_idx), 1], trans22[max(trans11_meas_idx), 1]),
    col = "purple",
    lty = 2,
    lwd = 2.5
)





# calculations-----------
bg_means11 <- c()
bg_means12 <- c()
bg_means21 <- c()
bg_means22 <- c()

meas_means11 <- c()
meas_means12 <- c()
meas_means21 <- c()
meas_means22 <- c()

for (i in 2:20) {
    bg_means11 <- append(bg_means11, mean(trans11[trans11_bg_idx, i]))
    bg_means12 <- append(bg_means12, mean(trans12[trans12_bg_idx, i]))
    bg_means21 <- append(bg_means21, mean(trans21[trans21_bg_idx, i]))
    bg_means22 <- append(bg_means22, mean(trans22[trans22_bg_idx, i]))
    
    meas_means11 <- append(meas_means11, mean(trans11[trans11_meas_idx, i]))
    meas_means12 <- append(meas_means12, mean(trans12[trans12_meas_idx, i]))
    meas_means21 <- append(meas_means21, mean(trans21[trans21_meas_idx, i]))
    meas_means22 <- append(meas_means22, mean(trans22[trans22_meas_idx, i]))
}

names(bg_means11) <- names(trans11[, 2:20])
names(bg_means12) <- names(trans12[, 2:20])
names(bg_means21) <- names(trans21[, 2:20])
names(bg_means22) <- names(trans22[, 2:20])

names(meas_means11) <- names(trans11[, 2:20])
names(meas_means12) <- names(trans12[, 2:20])
names(meas_means21) <- names(trans21[, 2:20])
names(meas_means22) <- names(trans22[, 2:20])

cps_11 <- meas_means11 - bg_means11
cps_12 <- meas_means12 - bg_means12
cps_21 <- meas_means21 - bg_means21
cps_22 <- meas_means22 - bg_means22

print(cps_11)


# Combine the vectors into a matrix or data frame
# Each vector becomes a row in the resulting data structure
mat <- data.frame(cps_11, cps_12, cps_21, cps_22)

# Calculate the element-wise mean for each vector element
mean_vec <- rowMeans(mat)

# Display the result
print(mean_vec)

df <- data.frame(elements_measured, mean_vec)

# Specify the file name and path
file_name <- "R_nist612.csv"  

# Write the data frame to a CSV file
write.csv(df, file = file_name, row.names = FALSE)
 
# dev.copy2pdf(file = "plots/boh.pdf")

