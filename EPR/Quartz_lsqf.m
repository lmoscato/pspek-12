%porcodddio

clear variables, close all;

[B_exp, I_exp, par] = eprload('data/gammaquartz');

B_exp = B_exp / 10 + 1.1438;

plot(B_exp, I_exp)

Sys.S = 1/2;

gx = 1.99996;
gz = 2.00181;
Sys.g = [gx gz];
Sys.HStrain = 2;


Exp.mwFreq = par.MF;
Exp.nPoints = numel(B_exp);
Exp.Range = [B_exp(1) B_exp(end)];
Exp.Harmonic = 1;

Opt.nKnots = 51;

SimOpt.nKnots = 51;
FitOpt.Method = 'simplex fcn';

Vary.g = [0.001 0.001];

esfit(@pepper, I_exp, Sys, Vary, Exp, SimOpt, FitOpt)
