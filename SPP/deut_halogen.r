#A-------------------------------------------------------------------
rm( list =ls ())

cur_dir <- getwd()
if (!grepl("/SPP", cur_dir, ignore.case = TRUE)) {
    setwd("./SPP")
}


deuterium <- read.table ("data/D_lamp_USB4F034531_1.txt", header = F, skip = 16)
halogen <- read.table ("data/H_lamp_USB4F034531_1.txt", header = F, skip = 16)
benzol_0 <- read.table ("data/Benzene_Absorbance_1.txt", header = F, skip =16)
benzol_20 <- read.table ("data/Benzene_Absorbance_5.txt", header = F, skip =16)
p_nitrophenol_0 <- read.table ("data/p-nitrophenol_Absorbance_1.txt", header = F, skip =16)
p_nitrophenol_20 <- read.table ("data/p-nitrophenol_Absorbance_5.txt", header = F, skip =16)
par (mar= c(5, 6, 3, 2) + 0.1)
# plotting deuterium and halogen

windows ( height =7, width =9)
par (mar= c(5, 6, 3, 2) + 0.1)
plot ( deuterium ,
type ="l", lwd =2, las =1, ann =F)
lines ( halogen , lwd =2, col ="coral1")
legend ("topleft", c("deuterium lamp", "halogen lamp"), lty=c(1 ,1) , lwd = c(3,3), col =c("black", "coral1"))
mtext ( side =1, text = expression ( lambda ~"/ nm"), line =3, cex =1.5)
mtext ( side =2, text ="Intensity / cps", line =4, cex =1.5)
axis (1, seq (0 ,1000 , by =50) , lab =NA , tck = -0.01)
axis (2, seq (0 ,50000 , by =5000) , lab =NA , tck = -0.01)
dev.copy2pdf ( file ="plots/A_deu_hal.pdf")
dev.off()

# plotting the boxcar settings
## benzene

if(0){ # SWITCH Benzene on or off

windows ( height =7, width =9)
par (mar= c(5, 6, 3, 2) + 0.1)
plot( benzol_0,
    type ="l", lwd =2, las =1, ann =F, ylim = c(-.5, 1.1))
lines ( benzol_20, lwd =2, col ="coral1")

mtext ( side =1, text = expression ( lambda ~"/ nm"), line =3, cex =1.5)
mtext ( side =2, text ="absorbance", line =4, cex =1.5)

axis (1, seq (0 ,1000 , by =50) , lab =NA , tck = -0.01)
axis (2, seq (-2,2, by =0.25) , lab =NA , tck = -0.01)

legend ("bottomright", c("boxcar width : 0","boxcar width : 20"), lty=c
(1 ,1) , lwd =c(3 ,3) , col =c("black","coral1"))

par (mar= c(20 , 18, 4, 3) + 0.1 , new=T)
P <- c(178 , -0.41 , 270 , 1.36)
plot ( benzol_0,
type ="l", lwd =2, las =1, ann =F, xlim =c(P[1] ,P [3]) ,ylim =c(P[2] ,P [4]) )
lines ( benzol_20, lwd =2, col ="coral1")
axis (1, seq (0 ,1000 , by =10) , lab =NA , tck = -0.01)
axis (2, seq (-2,2, by =0.25) , lab =NA , tck = -0.01)
par (mar= c(5, 6, 3, 3) + 0.1)
P <- locator (n=4)
rect (P$x[1] ,P$y[1] ,P$x[2] ,P$y[2] , border ="grey")
arrows (P$x[2] ,P$y[2] , P$x[3] , P$y[3] , col="grey",length =0)
arrows (P$x[2] ,P$y[1] , P$x[4] , P$y[4] , col="grey",length =0)
dev.copy2pdf ( file ="plots/A_benzol.pdf")
dev.off ()
}

## p-nitrophenol

if(0){

windows ( height =7, width =9)
par (mar= c(5, 6, 3, 2) + 0.1)
plot ( p_nitrophenol_0,
    type ="l",
    lwd =2,
    las =1,
    ann =F,
    ylim =c ( -0.4 ,1.1)
)

lines ( p_nitrophenol_20, lwd =2, col ="coral1")

mtext ( side =1, text = expression ( lambda ~"/ nm"), line =3, cex =1.5)
mtext ( side =2, text ="absorbance", line =4, cex =1.5)

axis (1, seq (0 ,1000 , by =50) , lab =NA , tck = -0.01)
axis (2, seq (-2,2, by =0.1) , lab =NA , tck = -0.01)

legend ("bottomright",
    c("boxcar width : 0","boxcar width : 20"),
    lty=c(1 ,1),
    lwd =c(3 ,3),
    col =c("black","coral1")
)
rect (178 ,-0.25 ,500 ,1.1 , border ="grey")

par(mar= c(19 , 25.5 , 4, 3) + 0.1 , new=T)
P <- c(178 , -0.25 , 500 , 1.1)


plot( p_nitrophenol_0,
    type ="l",
    lwd =2,
    las =1,
    ann =F,
    xlim =c(P[1] ,P [3]),
    ylim =c(P[2] ,P [4]) 
)
lines ( p_nitrophenol_20, lwd =2, col ="coral1")
axis (1, seq (0 ,1000 , by =25) , lab =NA , tck = -0.01)
axis (2, seq (-2,2, by =0.1) , lab =NA , tck = -0.01)

par(mar= c(5, 6, 3, 2) + 0.1)


P <- locator (n=2)
arrows (108.7 , 1.16 , P$x[1] , P$y[1] , col="grey",length =0)
arrows (109 , -1.5957, P$x[2] , P$y[2] , col="grey",length =0)
dev.copy2pdf ( file ="plots/A_p-nitrophenol.pdf")
dev.off()
}

# benzene with wavenumber
if(1){

windows ( height =7, width =9)
par(mar= c(5, 6, 3, 2) + 0.1)
nu <- 1/ benzol_0[1] * 10^7
plot ( unlist (nu),unlist ( benzol_0[ ,2]) ,
    type ="l", lwd =2, las =1, ann =F, xlim =c (43500 ,37000),
    ylim =c (0 ,0.5))

mtext ( side =1, text = expression ( tilde (nu)~"/ cm"^{ -1}) , line =3, cex =1.5)
mtext ( side =2, text ="Absorbance", line =4, cex =1.5)

axis (1, seq (0 ,100000 , by =500) , lab =NA , tck = -0.01)
axis (2, seq (-2,2, by =0.05) , lab =NA , tck = -0.01)

P <- locator (n=5)
points (P,
    lwd =2, pch =21 , bg="white"
)

nu0 <- c(P$x[2] -P$x[1] ,P$x[3] -P$x[2] ,P$x[4] -P$x[3] ,P$x[5] -P$x [4])
nu0.mean <- mean (nu0)
nu0.ci <- sd(nu0)*qt ((1+0.95) /2, df = length (nu0) -1)/ sqrt ( length (nu0))
arrows (P$x[1] , 0.45 , P$x[2] , 0.45 , length =0.1 , lwd =2, code =3)
mtext ( expression ( tilde (nu) [0]) , at =(P$x [1]+ P$x [2]) /2, line = -3)
dev.copy2pdf( file ="plots/A_benzol_zoom.pdf")

}