#C----------------------------------------------------------------
rm(list = ls())
library(colorspace)
library(httpgd)
par(mar = c(5,4,4,2) + 0.1)

cur_dir <- getwd()
if (!grepl("/SPP", cur_dir, ignore.case = TRUE)) {
    setwd("./SPP")
}


vanillin <- read.table("data/vanillin(C)_Absorbance_01.txt", header = F, skip =16)
o_vanillin <- read.table("data/o-vanillin(C)_Absorbance_01.txt", header = F, skip =16)
m_nitrophenol <- read.table ("data/m-nitrophenol(C)_Absorbance_01.txt", header = F, skip =16)
p_nitrophenol <- read.table ("data/p-nitrophenol(C)_Absorbance_01.txt", header = F, skip =16)
m3 <- read.table ("data/M3(C)_Absorbance_01.txt", header = F, skip =16)
m4 <- read.table ("data/M4(C)_Absorbance_01.txt", header = F, skip =16)


# the linear models
idx <- which(m3[,1] >= 320 & m3[,1] <= 500)
model_range2 <- c (850,2200)

model_m3 <- lm(m3[idx,2] ~ m_nitrophenol[idx, 2] + p_nitrophenol[idx , 2] + vanillin[idx,2])
model_m4 <- lm(m4[idx,2] ~ o_vanillin[idx, 2] + p_nitrophenol[idx, 2] + m_nitrophenol[idx, 2] + vanillin[idx, 2])



par(mfrow = c(2,2))

plot(vanillin,
    xlab = expression(lambda ~ "/ nm"),
    ylab = "Absorbance",
    main = "Vanillin",
    type = "l",
    las = 1, 
)

plot(o_vanillin,
    xlab = expression(lambda ~ "/ nm"),
    ylab = "Absorbance",
    main = "O-vanillin",
    type = "l",
    las = 1, 
)

plot(p_nitrophenol,
    xlab = expression(lambda ~ "/ nm"),
    ylab = "Absorbance",
    main = "P-nitrophenol",
    type = "l",
    las = 1,
    ylim = c(0,1) 
)

plot(m_nitrophenol,
    xlab = expression(lambda ~ "/ nm"),
    ylab = "Absorbance",
    main = "M-nitrophenol",
    type = "l",
    las = 1, 
)

layout(matrix(c(1,2,3,4), nrow = 2, byrow = T), heights = c(1,1))

palette <- qualitative_hcl(6, palette = "Dark")


# M3 FULL SPECTRUM
plot(m_nitrophenol,
    col = palette[1],
    las = 1,
    ylab = "Absorbance",
    xlab = expression(lambda ~ "/ nm"),
    type = "l",
    lwd = 1.5,
    cex = 1.5
)

lines(p_nitrophenol,
    col = palette[2],
    lwd = 1.5
)

lines(vanillin,
    col = palette[3],
    lwd = 1.5
)

lines(m3,
    col = palette[4],
    lwd = 1.5
)

prediction_m3 <- predict(model_m3)
prediction_m4 <- predict(model_m4)
lines(m3[idx,1], prediction_m3,
    lwd = 2,
    lty = 2,
    col = palette[5]
)

lines(m3[idx,1], residuals(model_m3) * 20,
    lwd = 2,
    col = palette[6]
)

legend("topright",
    legend = c("m-nitrophenol", "p-nitrophenol", "Vanillin", "Mixture", "Prediction", "Residuals (x20)"),
    lty = c(1,1,1,1,2,1),
    lwd = c(1.5,1.5,1.5,1,1),
    col = palette[1:6],
    bty = "n",

)

# M3 ZOOM ################################################3

plot(m3[idx,],
    type = "l",
    las = 1,
    ylim = c(-0.3,1.6),
    ylab = "Absorbance",
    xlab = expression(lambda ~ "/ nm"),
    col = palette [4]

)

lines(m3[idx, 1], prediction_m3,
    col = palette[5],
    lwd = 2,
    lty = 2
)

lines(m3[idx,1], residuals(model_m3) * 20,
    col = palette[6],
    lwd = 2
)

# M4 FULL SPECTRUM ############################################

palette2 <- sequential_hcl(7, palette = "viridis")

plot(m_nitrophenol,
    las = 1,
    type = "l",
    col = palette2[1],
    xlab = expression(lambda ~ "/ nm"),
    ylab = "Absorbance",
    lwd = 2
)


lines(p_nitrophenol,
    col = palette2[2],
    lwd = 2
)

lines(vanillin,
    col = palette2[3],
    lwd = 2
)

lines(o_vanillin,
    col = palette2[4],
    lwd = 2
)

lines(m4,
    col = palette2[5],
    lwd = 2
)

lines(m4[idx,1], prediction_m4,
    col = palette2[6],
    lwd = 2,
    lty = 2
)

lines(m4[idx,1], residuals(model_m4) * 20,
    col = palette2[7],
    lwd = 2,
    lty = 1
)

legend("topright",
    legend = c("m-nitrophenol", "p-nitrophenol", "vanillin", "o-vanillin", "Mixture", "Prediction", "Residuals (x20)"),
    lty = c(1,1,1,1,1,2,1),
    col = palette2,
    bty = "n"
)

# M4 ZOOM ####################################################

plot(m4[idx,],
    type = "l",
    las = 1,
    xlab = expression(lambda ~ "/ nm"),
    ylab = "Absorbance",
    lwd = 2,
    col = palette2[1],
    ylim = c(-0.2, max(m4[idx,2]))
)

lines(m4[idx,1], prediction_m4,
    col = palette2[6],
    lwd = 2,
    lty = 2
)

lines(m4[idx,1], residuals(model_m4) * 20,
    col = palette2[7],
    lwd = 2,
    lty = 1
)
