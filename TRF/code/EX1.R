rm(list = ls()) 

## Acceptor and donor emission and excitation spectra

DC <- 39.05849 #Dark counts

#Data + Intensity correction
#EXC 350 for 0.0% Acceptor 
infilename00 <- "~/Desktop/Team 12/day_one/0%_PL_EXC_375nm_Slit_1.7nm_EM_SLit_2.7nm_Att_100_0.1s/Custom_20231127_1337_0.dat"
EXC00 <- read.table(infilename00, skip = 35)

infilename00_EP <- "~/Desktop/Team 12/day_one/0%_PL_EXC_375nm_Slit_1.7nm_EM_SLit_2.7nm_Att_100_0.1s/Custom_20231127_1337_2.dat"
EXC00_EP <- read.table(infilename00_EP, skip = 35)

infilename00_DEC <- "~/Desktop/Team 12/day_one/0%_PL_EXC_375nm_Slit_1.7nm_EM_SLit_2.7nm_Att_100_0.1s/Custom_20231127_1337_1.dat"
EXC00_DEC <- read.table(infilename00_DEC, nrows = 401, skip = 162)

wave00_EXC <- EXC00[ ,1]                 
Int00_EXC <- ((EXC00[ ,2] - DC)/EXC00_EP[,2])*EXC00_DEC[,2]
Int00_EXC <- Int00_EXC/max(Int00_EXC) #417

#EM 470 for 0.0% Acceptor 
infilename_EM00 <- "~/Desktop/Team 12/day_one/0%_PLE_EXC__Slit_1.7nm_EM_480nm_SLit_2.7nm_Att_100_0.1s/Custom_20231127_1350_0.dat"
EM00 <- read.table(infilename_EM00, skip = 36)

infilename_EM00_EP <- "~/Desktop/Team 12/day_one/0%_PLE_EXC__Slit_1.7nm_EM_480nm_SLit_2.7nm_Att_100_0.1s/Custom_20231127_1350_1.dat"
EM00_EP <- read.table(infilename_EM00_EP, skip = 36)

wave00_EM <- EM00[ ,1]                 
Int00_EM <- ((EM00[ ,2] - DC)/EM00_EP[,2])
Int00_EM <- Int00_EM - min(Int00_EM)
Int00_EM <- Int00_EM/max(Int00_EM)

#EXC 450 for 0.8% Acceptor 
infilename08 <- "~/Desktop/Team 12/day_one/0.8%_PL_EXC_425nm_Slit_0.5nm_EM_SLit_1.7nm_Att_100_0.1s/Custom_20231127_1406_0.dat"
EXC08 <- read.table(infilename08, skip = 37)

infilename08_EP <- "~/Desktop/Team 12/day_one/0.8%_PL_EXC_425nm_Slit_0.5nm_EM_SLit_1.7nm_Att_100_0.1s/Custom_20231127_1406_2.dat"
EXC08_EP <- read.table(infilename08_EP, skip = 37)

infilename08_DEC <- "~/Desktop/Team 12/day_one/0.8%_PL_EXC_425nm_Slit_0.5nm_EM_SLit_1.7nm_Att_100_0.1s/Custom_20231127_1406_1.dat"
EXC08_DEC <- read.table(infilename08_DEC, nrows = 359, skip = 204) #262

wave08_EXC <- EXC08[ ,1]                 
Int08_EXC <- ((EXC08[ ,2] - DC)/EXC08_EP[,2])*EXC08_DEC[,2]
Int08_EXC <- Int08_EXC/max(Int08_EXC) #616

#EM 611 for 0.8% Acceptor 
infilename_EM08 <- "~/Desktop/Team 12/day_one/0.8%_PLE_EXC_Slit_0.5nm_EM_610nm_SLit_1.7nm_Att_100_0.1s/Custom_20231127_1413_0.dat"
EM08 <- read.table(infilename_EM08, skip = 36)

infilename_EM08_EP <- "~/Desktop/Team 12/day_one/0.8%_PLE_EXC_Slit_0.5nm_EM_610nm_SLit_1.7nm_Att_100_0.1s/Custom_20231127_1413_1.dat"
EM08_EP <- read.table(infilename_EM08_EP, skip = 36)

wave08_EM <- EM08[ ,1]                 
Int08_EM <- (EM08[ ,2] - DC)/EM08_EP[,2]
Int08_EM <- Int08_EM - min(Int08_EM)
Int08_EM <- Int08_EM/max(Int08_EM) #576

#Plotting
plot(wave00_EXC, Int00_EXC, type="l", las=0, 
     col = "#4999de", 
     xlab=expression("Wavelength / nm"), 
     ylab=expression(" Normalized Intensity / a.u."),
     xlim = c(250,800),
     ylim = c(0, 1.2), 
     mgp=c(2.5,1,0)
)
lines(wave00_EM, Int00_EM, col = "#7c15c0")
lines(wave08_EXC, Int08_EXC, col = "#ff3122")
lines(wave08_EM, Int08_EM, col = "#FF9800")

legend("topright", legend=c(expression("Donor emission"), 
expression("Donor absorption"), expression("Acceptor emission"), 
           expression("Acceptor absorption")),
lty=c(1,1, 1, 1), col=c("#4999de", "#7c15c0", "#ff3122", "#FF9800"),
y.intersp=2, cex = 0.5, inset=c(0.06,0.03), bty = "n")

dev.copy2pdf(file="Exp1.pdf", width=7, height= 6) #Saving
