% gamma-irradiated Quartz Glas

% Attention: units of magnetic field!


clear all, close all    % clear work space and close all figures

% load experimental spectrum
[B_exp,I_exp,par] = eprload('data/gammaquartz');

B_exp = B_exp/10 + 1.1438;   % Gauss to mT
                    % don't forget to add the field correction!

%%
error = 5e-5;
% Simulate spectrum:
Sys.S = 0.5;                              % Electron Spin quantum number
gx = 2.00049;
gy = gx;  
gz = 2.00181;
Sys.g = [gx gy gz];                       % axial g-tensor (gx = gy, gz)
Sys.HStrain = 2;                          % Line broadening (by unresolved HF couplings)

Sys2 = Sys;
gx2 = gx - error;
gz2 = gz;
Sys2.g = [gx2 gz];

Sys3 = Sys;
gx3 = gx + error;
Sys3.g = [gx3 gz];

Exp.mwFreq=par.MF;                        % mw frequency, taken from experimental data 
Exp.Range = [B_exp(1) B_exp(end)] ;       % Simulation range, careful: don't make simulation range to large
Exp.nPoints = numel(B_exp);               % Number of Points in the simulation
Exp.Harmonic = 1;                         % 1: derivative spetrum

Opt.nKnots = 51;                          % Defines number of orientations

[B_sim,I_sim] = pepper(Sys,Exp,Opt) ;     % function to simulate solid spectrum
[B_sim2, I_sim2] = pepper(Sys2, Exp, Opt);
[B_sim3, I_sim3] = pepper(Sys3, Exp, Opt);


% Plot 1
figure(1);                                % open a figure window
clf;                                      % clear figure content
hold on, box off                           % plot several lines in one plot and make box around figure

plot(B_exp,I_exp/max(abs(I_exp)))     % plot of normalized experimental spectrum
plot(B_sim,I_sim/max(abs(I_sim)))     % plot of normalized simulated spectrum

xlim([min(B_exp) max(B_exp)]) ;           % set x-axis
ylim([-1.04 1.04])                        % set y-axis
xlabel('Field (mT)')                      % axis labels
ylabel('Norm. Intensity a.u.')
legend('Exp','Sim')                         % legend
fontname('CMU Serif');
print('plots/quartz', '-dpng');

figure(2);

hold on, box off

plot(B_exp,I_exp/max(abs(I_exp)))     % plot of normalized experimental spectrum
plot(B_sim,I_sim/max(abs(I_sim)))
plot(B_sim2,I_sim2/max(abs(I_sim2)))     % plots of errors for appendix
plot(B_sim3,I_sim3/max(abs(I_sim3))) 

xlim([min(B_exp) max(B_exp)]) ;           % set x-axis
ylim([-1.04 1.04])                        % set y-axis
xlabel('Field (mT)')                      % axis labels
ylabel('Norm. Intensity a.u.')
legend('Exp','Sim','g_{||} - 5 \times 10^{-5}','g_{||} + 5 \times 10^{-5}')                         % legend
fontname('CMU Serif');
print('plots/quartzerror', '-dpng');