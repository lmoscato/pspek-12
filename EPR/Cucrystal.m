% Cu(sal)2 single crystal CW-EPR

clear variables, close all;

[B_1, I_1,par_1] = eprload('data/cusingle_1'); % import data for max RG
[B_2, I_2, par_2] = eprload('data/cusingle_2'); % import data for min RG

B_1 = B_1 / 10; % convert to mT
B_2 = B_2 / 10; 

tiledlayout(1,2);

hold off, box off;

set(gcf,'Position',[100 100 1500 900])
nexttile
plot(B_2, I_2);
% title('Strong pitch with min receiver gain');
xlabel('Magnetic Field [mT]');
ylabel('Intensity a.u.');
xlim([B_1(1) 360])

fontname('CMU Serif');

nexttile;

plot(B_1, I_1);
xlim([B_1(1) 360])
% title('Strong pitch with max receiver gain');
xlabel('Magnetic Field [mT]');
ylabel('Intensity a.u.');
fontname('CMU Serif');
fontsize(scale = 1.5)


print('plots/cucrystal', '-dpng');