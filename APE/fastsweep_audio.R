#WAV-audiofile lesen
rm(list=ls())
library(tuneR)
library(httpgd)
cur_dir <- getwd()

if (!grepl("/APE", cur_dir, ignore.case = TRUE)) {
    setwd("./APE")
}

source("APECPVutils.R")

filename <- "fastsweep"
WAV.filename <- sprintf("audio/%s.wav", filename)

WAV <- read.WAV(WAV.filename)

sr <- WAV$Rate # [Hz]
dt <- 1 / sr # dwell time [s]

t <- WAV$Time
s <- WAV$Signal


#t <-t - trigger(t,s, 1)
N <- 2^12
par(mfrow=c(1,2))


# windows() per aprire finestra

plot(t,s, type = "l", xlim=c(0, 0.1), ylim=c(-1, 1),
xlab=expression(italic(t)*"/s"), 
ylab=expression(italic(s)*"/V"),
main="Time domain s(t)")

# idx<- which(t< -0.005)

# t<- t[-idx][1:N]
# s<- s[-idx][1:N]


DFT <- myfft(s,sr)

f<- DFT$Freq
p<- DFT$Pow

plot(f,p, type = "l", xlim=c(1000, 5000), ylim=c(),
xlab=expression(italic(f)*"/Hz"), 
ylab=expression(italic(P)*"/a.u."),
main="Frequency domain P(f)")

# #pdf

pdf.filename <- sprintf("plots/%s.pdf", filename)
dev.copy2pdf(file=pdf.filename, width=9, height=5)



