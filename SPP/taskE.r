#E----------------------------------------------------------------
rm( list =ls ())
library (colorspace)
library (chemCal)

cur_dir <- getwd()
if (!grepl("/SPP", cur_dir, ignore.case = TRUE)) {
    setwd("./SPP")
}

skip <- 16
sol1 <- read.table("data/TB_Temp_Absorbance_1.txt", header = F, skip = skip)
sol2 <- read.table("data/TB_Temp_Absorbance_2.txt", header = F, skip = skip)
sol3 <- read.table("data/TB_Temp_Absorbance_3.txt", header = F, skip = skip)
sol4 <- read.table("data/TB_Temp_Absorbance_4.txt", header = F, skip = skip)
sol5 <- read.table("data/TB_Temp_Absorbance_5.txt", header = F, skip = skip)
sol6 <- read.table("data/TB_Temp_Absorbance_6.txt", header = F, skip = skip)
sol7 <- read.table("data/TB_Temp_Absorbance_7.txt", header = F, skip = skip)
sol8 <- read.table("data/TB_Temp_Absorbance_8.txt", header = F, skip = skip)
sol9 <- read.table("data/TB_Temp_Absorbance_9.txt", header = F, skip = skip)
sol10 <-read.table("data/TB_Temp_Absorbance_10.txt",header = F, skip = skip)
solHB <-read.table("data/TB_HCl_Absorbance_01.txt", header = F, skip = skip)
solB <- read.table("data/TB_H2O_Absorbance_01.txt", header = F, skip = skip)
T <- c(16.2, 19, 22.2, 25.0, 29.2, 32.5, 36, 40.1, 45.0, 50) + 273.15

Q <- c(8 ,309 , 553 , 809 , 819 , 1562 , 1897)
# the linear models
idx <- c (809:1897)
#c (1632 ,1820)
model1 <- lm(sol1[idx, 2] ~ solB[idx, 2] + solHB[idx, 2])
model2 <- lm(sol2[idx, 2] ~ solB[idx, 2] + solHB[idx, 2])
model3 <- lm(sol3[idx, 2] ~ solB[idx, 2] + solHB[idx, 2])
model4 <- lm(sol4[idx, 2] ~ solB[idx, 2] + solHB[idx, 2])
model5 <- lm(sol5[idx, 2] ~ solB[idx, 2] + solHB[idx, 2])
model6 <- lm(sol6[idx, 2] ~ solB[idx, 2] + solHB[idx, 2])
model7 <- lm(sol7[idx, 2] ~ solB[idx, 2] + solHB[idx, 2])
model8 <- lm(sol8[idx, 2] ~ solB[idx, 2] + solHB[idx, 2])
model9 <- lm(sol9[idx, 2] ~ solB[idx, 2] + solHB[idx, 2])
model10<-lm(sol10[idx, 2] ~ solB[idx, 2] + solHB[idx, 2])

coeffB <- c(summary(model1)$coeff[2, 1], summary(model2)$coeff[2, 1], summary(model3)$coeff[2, 1], summary(model4)$coeff[2, 1], summary(model5)$coeff[2, 1], summary(model6)$coeff[2, 1],   summary(model7)$coeff[2, 1], summary(model8)$coeff[2, 1], summary(model9)$coeff[2, 1], summary(model10)$coeff[2, 1]
)

coeffHB <- c(summary(model1)$coeff[3, 1], summary(model2)$coeff[3, 1], summary(model3)$coeff[3, 1], summary(model4)$coeff[3, 1], summary(model5)$coeff[3, 1], summary(model6)$coeff[3, 1], summary(model7)$coeff[3, 1], summary(model8)$coeff[3, 1], summary(model9)$coeff[3, 1], summary(model10)$coeff[3, 1])

R <- 8.31446261815324
pH <- 1.548136 # from previous experiment
pKa <- pH - log10(coeffB / coeffHB)
inv_T <- 1/T
model <- lm(pKa ~ inv_T)
slope <- summary(model)$coeff[2 ,1]
slope.ci <- summary(model)$coeff[2 ,2] * qt (0.975 , df = df.residual(model))
intercept <- summary(model)$coeff[1 ,1]
intercept.ci <- summary(model)$coeff[1 ,2] * qt (0.975 , df = df.residual(model))
x_test <- seq (0.003, 0.0035, length =1000)
conf_interval <- predict(model, newdata = list(inv_T = x_test), interval = "confidence", level =0.95)

deltaH <- slope *R/ log10 (exp (1))
deltaH.ci <- slope.ci* R/ log10(exp(1))

deltaS <- -intercept * R / log10(exp(1))
deltaS.ci <- intercept.ci * R / log10(exp(1))


# plotting all spectra
if (1) {
windows ( height =7, width =9)
par(mar= c(5, 6, 3, 2) + 0.1)
col <- sequential_hcl(10, palette = "Viridis")

plot (NA , las =1, ann =F, xlim =c(180 , 900) ,ylim =c(0 ,2.5))

lines(sol1, lwd = 2, col = col[1])
lines(sol2, lwd = 2, col = col[4])
lines(sol3, lwd = 2, col = col[3])
lines(sol4, lwd = 2, col = col[2])
lines(sol5, lwd = 2, col = col[5])
lines(sol6, lwd = 2, col = col[6])
lines(sol7, lwd = 2, col = col[7])
lines(sol8, lwd = 2, col = col[8])
lines(sol9, lwd = 2, col = col[9])
lines(sol10, lwd = 2, col = col[10])

lines(solHB, lty =2)
lines(solB, lty =3)

mtext( side =1, text = expression ( lambda ~"/ nm"), line =3, cex =1.5)
mtext( side =2, text = "Absorbance", line =4, cex =1.5)

axis(1, seq(0 ,1000, by =50), lab =NA, tck = -0.01)
axis(2, seq( -2 ,10, by =0.1), lab =NA, tck = -0.01)
axis(3, seq(0 ,1000, by =100), lab =NA, tck = 0.02)
axis(4, seq( -2 ,10, by =0.2), lab =NA, tck = 0.01)
axis(3, seq(0 ,1000, by =50), lab =NA, tck = 0.01)
axis(4, seq( -2 ,10, by =0.1), lab =NA, tck = 0.01)

# arrows( sol1 [idx[1],1], 0.95 ,sol1 [idx[2], 1], 0.95, angle=90, lwd =2, code =3, length =0.05)
legend ("topright", c("T = 16.2 C", "T = 50 C","HB", "B"), col =c(col[1],col[10], "black", "black"),lty=c(1 ,1 ,2 ,3) ,lwd=c(2 ,2 ,2 ,2))
dev.copy2pdf( file = "plots/E_ spectra.pdf")
dev.off()
}

# plotting vant hoff
if (1) {
windows ( height =7, width =9)
par(mar= c(5, 6, 3, 2) + 0.1)
plot (inv_T, pKa,
    las =1,
    ann =F,
)
# points (inv_T, pKa, cex =1.2 , lwd =2)
# points (1/T_exc, pKa_exc, cex = 1.2, lwd =2, col = "grey")

abline (model, lwd =2)
lines (x_test , conf_interval[,2])
lines (x_test , conf_interval[,3])
mtext ( side = 1, text = expression (frac(1,T)~"/"~ "K"^{-1}) , line =4, cex =1.5)
mtext ( side = 2, text = "pKa", line =4, cex =1.5)
axis (1, seq (0 ,10 , by =0.000025) , lab =NA , tck = -0.01)
axis (2, seq ( -2 ,10 , by =0.025) , lab =NA , tck = -0.01)
axis (3, seq (0 ,10 , by =0.00005) , lab =NA , tck =0.02)
axis (4, seq ( -2 ,10 , by =0.05) , lab =NA , tck =0.02)
axis (3, seq (0 ,10 , by =0.000025) , lab =NA , tck =0.01)
axis (4, seq ( -2 ,10 , by =0.025) , lab =NA , tck =0.01)
dev.copy2pdf( file ="plots/E_linear_plot.pdf")
dev.off()
}