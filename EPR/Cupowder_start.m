% Cu(sal)2

% Attention: units of magnetic field!

clear all, close all    % clear work space and close all figures


% load experimental spectrum
[B_exp,I_exp,par]=eprload('data/cupowder');


B_exp=B_exp/10 + 1.1438;     % Gauss to mT
                    % don't forget to add the field correction!

%%

% Simulate spectrum:
Sys.S = 0.5;                            % Electron Spin quantum number
gx = 2.05533;
gy = gx;
gz = 2.18771;
Sys.g = [gx gz];                       % axial g-tensor (gx = gy, gz)
Sys.Nucs = '63Cu,14N,14N';              % Nuclei 
A_Cu = [104.529 638.788];                        % Copper Couplings
A_N = [51.2684 50.4274] ;                          % Nitrogen Couplings (equivalent nuclei)
Sys.A = [A_Cu; A_N; A_N];               % Put couplings in one field
Sys.HStrain = [54.322 28];                  % Line broadening (by unresolved HF couplings)

Exp.mwFreq = par.MF;                    % mw frequency, taken from experimental data 
Exp.Range = [B_exp(1) B_exp(end)];      % Simulation range, careful: don't make simulation range to largee
Exp.Harmonic = 1;                       % 1: derivative spetrum
Exp.nPoints = numel(B_exp);             % Number of Points in the simulation

Opt.nKots = 51;                         % Defines number of orientations

[B_sim,I_sim] = pepper(Sys,Exp,Opt) ;   % function to simulate solid spectrum

% % optional: use fitting algorithm once reasonable start parameters are found 
% Vary.g = [0.05 0.05];
% Vary.A = [10 10; 10 10; 10 10];
% Vary.HStrain = [10];
% esfit('pepper',I_exp,Sys,Vary,Exp);
%%
% Plot
figure(1);                              % open a figure window
clf;                                    % clear figure content
hold on, box off                         % plot several lines in one plot and make box around figure

plot(B_exp,I_exp/max(abs(I_exp)))   % plot of experimental spectrum
plot(B_sim,I_sim/max(abs(I_sim)))   % plot of simulated spectrum

xlim([B_exp(1) B_exp(end)]) ;           % set x-axis
ylim([-1.04 1.04])                      % set y-axis
xlabel('Field (mT)')                    % axis labels
ylabel('Norm. Intensity a.u.')
legend('Exp','Sim')                     % legend
fontname('CMU Serif')

print('plots/cupowder_sim', '-dpng')