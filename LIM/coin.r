rm(list = ls())

# PACKAGES #########################
library(colorspace)
library(httpgd)
library(magicaxis)
library(httpgd)

# OTHER RULES ##################################
cur_dir <- getwd()
if (!grepl("/LIM", cur_dir, ignore.case = TRUE)) {
    setwd("./LIM")
}


# DATA EXTRACTION ###########################################
bg <- read.table(file = "data/COIN_BG.D/COIN_BG.d#/COIN_BG.tab", skip = 9, header = TRUE)
coin <- read.table(file = "data/COIN.D/COIN.d#/COIN.tab", skip = 9, header = TRUE)

masses <- bg[,1]
intensities <- coin[,2] - bg[,2]

zoom_idx <- which(masses > 55 & masses < 70)

# PLOTTING BG SPECTRUM #########################################
par(mfrow = c(1,1))
plot(masses, bg[,2],
    type = "h",
    log = "y",
    xaxt = "n",
    yaxt = "n",
    ylab = "Intensity [CPS]",
    xlab = "Mass [Da]",
    xlim = c(8, 235),
    cex.lab = 1.3
)
magaxis(2, hersh = TRUE, minorn = 9, cex.axis = 1.3)
magaxis(1, hersh = TRUE, majorn = 10, minorn = 10, cex.axis = 1.3)


# PLOTTING FULL SPECTRUM ############################################
par(mfrow = c(2,1))
plot(masses, intensities,
    type = "h",
    log = "y",
    xaxt = "n",
    yaxt = "n",
    ylab = "Intensity [CPS]",
    xlab = "Mass [Da]",
    xlim = c(8, 235),
    cex.lab = 1.3
)
magaxis(2, hersh = TRUE, minorn=9, cex.axis = 1.3)
magaxis(1, hersh = TRUE, majorn = 10, minorn = 10, cex.axis = 1.3)


plot(masses[zoom_idx], intensities[zoom_idx],
    type = "h",
    log = "y",
    xaxt = "n",
    yaxt = "n",
    ylab = "Intensity [CPS]",
    xlab = "Mass [Da]",
    cex.lab = 1.3
)
magaxis(2, hersh = TRUE, minorn=9, cex.axis = 1.3)
magaxis(1, hersh = TRUE, majorn = 10, minorn = 10, cex.axis = 1.3)

# TRANSIENT PLOTTING ############################################
par(mfrow = c(1,1))

palette2 <- choose_palette()
palette <- palette2(19)
trans_data <- read.csv(file = "data/COIN_TR.D/COIN_TR.d#/COIN_TR.csv", skip = 3, sep = ",")
trans_data[,2:19] <- trans_data[,2:19] * 100

plot(trans_data[, 1], trans_data[, 2],
    col = palette[1],
    type = "l",
    ylim = c(1e2, 1e10),
    xlab = "Time [s]",
    log = "y",
    ylab = "Intensity [CPS]",
    las = 1,
    yaxt = "n",
    xaxt = "n",
    cex.lab = 1.3
)
magaxis(1, hersh = TRUE, family = "serif", minorn = 5, cex.axis = 1.3) #pretty axes for plot
magaxis(2, hersh = TRUE, family = "serif", minorn = 9, cex.axis = 1.3)

for (i in 3:19) { # plot everything with for loop
    lines(trans_data[, 1], trans_data[, i],
        col = palette[i - 1],
        log = "y"
    )
}

legend("topright",
    legend = names(trans_data[,2:19]),
    lty = 1,
    lwd = 2,
    col = palette
)

