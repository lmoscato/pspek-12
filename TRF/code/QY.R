rm(list = ls()) 

## Quantum yield

#Data absolute quantum yield
x <- c(0.0, 0.1, 0.2, 0.4, 0.6, 0.8)
y <- c(0.000000, 0.164238,0.311644,0.472109, 0.547816, 0.57745)
#y <- c(0.57745, 0.547816,.472109,0.311644, 0.164238, 0.000000)
#y <- y/0.57745 #Realtive QY

xlin <- seq(0, 3, 0.01)
z <- 0.016/(0.016+xlin) #Fitting from Matlab

#Plotting
plot(x, y, type="p", pch=20, 
     xlab = expression("Total acceptor concentration / %"), 
     ylab = expression("Realtive quantum yield / a.u."), mgp=c(2.5,1,0),
     xlim = c(0, 1))
lines(xlin, z, lty = 3)

legend("topright",
       legend=c(
                expression("Measured points"), expression("Fitting")),
       lty=c(0, 3), pch = c(20, NA),
       y.intersp=1, cex = 0.7, inset=c(0.03,0.03))

dev.copy2pdf(file="QY.pdf", width=7, height= 6) #Saving