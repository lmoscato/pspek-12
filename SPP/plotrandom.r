rm(list = ls())

library(httpgd)

data <- read.table("data/m-nitrophenol(C)_Absorbance_01.txt", header = F, skip = 16)

plot(data, type = "l")