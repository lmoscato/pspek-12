#WAV-audiofile lesen
rm(list=ls())
library(tuneR)
library(httpgd)
cur_dir <- getwd()

if (!grepl("/APE", cur_dir, ignore.case = TRUE)) {
    setwd("./APE")
}

source("APECPVutils.R")



#x(t)###################


filename1 <- "REPte1"
WAV.filename1 <- sprintf("audio/%s.wav", filename1)

WAV <- read.WAV(WAV.filename1)

sr <- WAV$Rate # [Hz]
dt <- 1 / sr # dwell time [s]

tx <- WAV$Time
x <- WAV$Signal

tx <-tx - trigger(tx,x, 0.02)
idx<- which(tx>-0.005 & tx< 1) #relevant data
tx<- tx[idx]
x<- x[idx]


#h(t)###########################################

filename <- "deltapuls_rec"
WAV.filename <- sprintf("recordings/%s.wav", filename)

WAV <- read.WAV(WAV.filename)

th <- WAV$Time
h <- WAV$Signal

th <-th - trigger(th,h, 0.02)
idx<- which(th>-0.005 & th< 1) #relevant data
th<- th[idx]
h<- h[idx]



#y(t)###########################################

filename2 <- "REPte1_rec"
WAV.filename2 <- sprintf("recordings/%s.wav", filename2)

WAV <- read.WAV(WAV.filename2)

ty <- WAV$Time
y <- WAV$Signal
y<- y/max(abs(y)) #normierung

ty <-ty - trigger(ty,y, 0.02)
idx<- which(ty>-0.005 & ty< 1) #relevant data
ty<- ty[idx]
y<- y[idx]



######

# windows() per aprire finestra

par(mfrow=c(2,2))
plot(th,h, type = "l", xlim=c(0, 0.1), 
xlab=expression(italic(t)*"/s"), 
ylab=expression(italic(h)*"/V"),
main="impuls response h(t)")

plot(tx,x, type = "l", xlim=c(),
xlab=expression(italic(t)*"/s"), 
ylab=expression(italic(x)*"/V"),
main=" simulation x(t)")

plot(ty,y, type = "l", xlim=c(),
xlab=expression(italic(t)*"/s"), 
ylab=expression(italic(y)*"/V"),
main=" y(t)")

#antwort berechnene mit faltung integral y =x * h

yc <- myconv(x,h)
yc <- yc/max(abs(yc))
t <- seq(from=0, by=dt, length.out= length(yc))
t <- t - trigger(t, yc, 0.02)
idx<- which(t>-0.005 & t< 1) #relevant data
tyc<- t[idx]
yc<- yc[idx]

plot(tyc,yc, type = "l", xlim=c(),
xlab=expression(italic(t)*"/s"), 
ylab=expression(italic(y)*"/V"),
main=" y(t) calculated")
#lines(ty,y, col="red")

# ########################################

par(mfrow=c(1,2))
plot(tyc,yc, type = "l", xlim=c(0.1, 0.12),
xlab=expression(italic(t)*"/s"), 
ylab=expression(italic(y)*"/V"), col= "#e2692d",
main=" y(t) calculated")
lines(ty,y, col="black")



plot(tyc,yc, type = "l", xlim=c(),
xlab=expression(italic(t)*"/s"), 
ylab=expression(italic(y)*"/V"), col= "#e2692d",
main=" y(t) calculated")
lines(ty,y, col="black")

###################################################

#pdf("plots/calc_REP2te.pdf", width = 9, height = 5)  # Specify the filename and dimensions

par(mfrow=c(1,2))
plot(tyc,yc, type = "l", xlim=c(0,0.1),
xlab=expression(italic(t)*"/s"), 
ylab=expression(italic(y)*"/V"))
#lines(ty,y, col="red")

DFT <- myfft(yc,sr)

f<- DFT$Freq
p<- DFT$Pow

plot(f,p, type = "l", xlim=c(0, 4000), ylim=c(0, 4000),
xlab=expression(italic(f)*"/Hz"), 
ylab=expression(italic(P)*"/a.u."))

#dev.off()
