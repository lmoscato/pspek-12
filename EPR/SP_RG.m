% Receiver gain

clear variables; close all

[B_max, I_max,par_max] = eprload('data/SP_maxRG'); % import data for max RG
[B_min, I_min, par_min] = eprload('data/SP_minRG'); % import data for min RG

B_max = B_max / 10; % convert to mT
B_min = B_min / 10; 

tiledlayout(1,2);

hold off; box off;
set(gcf,'Position',[100 100 1500 900])
nexttile

plot(B_min, I_min);
title('Strong pitch with min receiver gain');
xlabel('Magnetic Field [mT]');
ylabel('Intensity a.u.');

fontname('CMU Serif');

nexttile;

plot(B_max, I_max);
title('Strong pitch with max receiver gain');
xlabel('Magnetic Field [mT]');
ylabel('Intensity a.u.');
fontname('CMU Serif');
fontsize(scale = 1.5)


print('plots/SP_RG', '-dpng');

