% Conversion Time, Strong Pitch

clear variables, close all;

% Data reading
[B_0, I_0, par_0] = eprload('data/SP_highCT');
[B_1, I_1, par_1] = eprload('data/SP_lowCT');
[B_2, I_2, par_2] = eprload('data/SP_lowestCT');

B_0 = B_0 / 10; % Conversion to mT
B_1 = B_1 / 10;
B_2 = B_2 / 10;

figure(1);
hold on, box off;

plot(B_0, I_0)
plot(B_1, I_1)
plot(B_2, I_2)

fontname('CMU Serif');
xlabel('Magnetic Field [mT]');
ylabel('Intensity [a.u.]');
xlim([B_0(1) B_0(end)]);
lg = legend(string(par_0.RTC), string(par_1.RTC), string(par_2.RTC));
title(lg, 'Conversion Time [ms]');

ax = axes('position', [.2 .2 .3 .3]);

hold on, box on, axis tight;
idx = B_0 > 349 & B_0 < 352; 
plot(B_0(idx), I_0(idx))
plot(B_1(idx), I_1(idx))
plot(B_2(idx), I_2(idx))


ylim([-2500 2500])
fontname('CMU Serif')

print('plots/SP_CT', '-dpng')

