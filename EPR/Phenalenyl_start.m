%%
%  Phenalenyl radical

% Attention: units of magnetic field!

clear all, close all    %#ok<CLALL> % clear work space and close all figures


% load experimental spectrum
[B_exp,I_exp,par]=eprload('data/phenalenyl');

B_exp=B_exp/10 + 1.1438;             % easyspin is calculating with mT not with Gauss
                            % don't forget to add the field correction!

%%

% Simulate spectrum:
Sys.S = 0.5;                            % Electron Spin quantum number
gx = 2.00386;
gy = gx;
gz = gx;
Sys.g = gx;                     % isotropic g value (gx = gy = gz)
Sys.Nucs = '1H,1H';                     % Nuclei (equivalent sets)
Sys.n=[6,3];                            % Numbers of Nuclei in each set
A_H1 = mt2mhz(0.630);                       % HF coupling of six equivalent nuclei in MHz
A_H2 = mt2mhz(0.1820);                       % HF coupling of three equivalent nuclei in MHz
Sys.A = [A_H1 A_H2];                    % HF Coupling for each set, in MHz
Sys.lw = 0.04;                          % Line broadening in liquids

Exp.mwFreq=par.MF;                      % mw frequency, taken from experimental data 
Exp.Range=[B_exp(1) B_exp(end)];        % Simulation range, careful: don't make simulation range to large
Exp.Harmonic=1;                         % 1: derivative spetrum
Exp.nPoints=2048 * 2;                       % Number of Points in the simulation

SimOpt.Method = 'perturb';
FitOpt.Method = 'simplex fcn';

Vary.g = 0.001;
Vary.A = [0.01 0.01];

[B_sim,I_sim] = garlic(Sys,Exp);        % function to simulate liquid spectrum
%esfit(@garlic, I_exp, Sys, Vary, Exp, SimOpt, FitOpt)

%%yxx

% Plot
figure(1);                              % open a figure window
clf;                                    % clear figure content
hold on, box on                         % plot several lines in one plot and make box around figure

plot(B_exp,I_exp/max(abs(I_exp)))   % plot of experimental spectrum, normalized
plot(B_sim,I_sim/max(abs(I_sim)))   % plot of simulated spectrum, normalized

xlim([min(B_exp) max(B_exp)]) ;         % set x-axis
ylim([-1.04 1.04])                      % set y-axis
xlabel('Field (mT)')                    % axis labels
ylabel('Norm. Intensity a.u.')
legend('Exp','Sim')                     % legend

print('plots/phenalenyl', '-dpng')