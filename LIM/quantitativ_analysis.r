remove(list = ls()) # Usual stuff.

# packages ---------------------------------------------warnin
library("colorspace")
library("extrafont")
library("magicaxis")
library("errors")
library("httpgd")

# other rules -------------------------------------------

# This if statment verifies if the working directory is set correctly, if it's not, it sets
# the correct one. (pspek-12/LIM)
cur_dir <- getwd()
if (!grepl("/LIM", cur_dir, ignore.case = TRUE)) {
    setwd("./LIM")
}
source("../minorticks.r")
source("find_error.r")
rm(cur_dir)
options(errors.notation = "plus-minus", errors.digits = 2) # write errors as plus minus after value

par(family = "CM Roman") # LaTeX font
# x11(width = 8, height = 6) #open device

# data reading ------------------------------------------------------------------
R610 <- read.csv(sep = ",", file = "R_nist610.csv") #read data from file
#C610 <- read.csv(sep = ",", file = "data/nist610data.csv")
R612 <- read.csv(sep = ",", file = "R_nist610.csv")


lit_data <- read.csv(sep = ",", file = "data/nist610data.csv", header = TRUE)
lit_concentrations <- lit_data[, 2]
names(lit_concentrations) <- lit_data[, 1]
uncertainties <- lit_data[, 3]
names(uncertainties) <- lit_data[, 1]

elements_measured <- c("Li", "Na2O", "SiO2", "CaO", "Sc", "Ti", "Mn", "Co", "Ga", "As", "Y", "Nb", "Pd", "Sb", "Cs", "Ho", "Au", "Th", "U")





# IS data for IS = Si ------------------------------------------------------------

R612_Si <- 1193086.55724923
C612_Si<- 0.00337042
errors(C612_Si) <-  0.00002805
R610_Si <- 1282237.7755588
C610_Si <- 0.00325823
errors(C610_Si) <- 0.00002337



lit_data <- read.csv(sep = ",", file = "data/nist610data.csv", header = TRUE)
lit_concentrations <- lit_data[, 2]
names(lit_concentrations) <- lit_data[, 1]
uncertainties <- lit_data[, 3]
names(uncertainties) <- lit_data[, 1]

C610 <- lit_concentrations[elements_measured]
errors(C610) <- uncertainties[elements_measured]


# Initialize an empty vector S
S <- numeric(length(R610))

# Calculate the elements of S using a for loop
S <- R610[,2] / C610 * (R612_Si/R610_Si)*(C610_Si/C612_Si)

# Display the vector S
print(S)


df <- data.frame(elements_measured, S, errors(S))

# Specify the file name and path
file_name <- "S_612.csv"

# Write the data frame to a CSV file
write.csv(df, file = file_name, row.names = FALSE)

#concentrations calculation -------------------------------------------------------
R_612 <- read.csv(sep = ",", file = "R_nist612.csv")

C_612 <- R_612[,2]/S


dff <- data.frame(elements_measured, C_612, errors(C_612))

# Specify the file name and path
file_name <- "C_612.csv"

# Write the data frame to a CSV file
write.csv(dff, file = file_name, row.names = FALSE)

#LOD 

n_b <- 152
n_a <- 239
LOD <- ((3*errors(S))/(S))*sqrt(1/n_b+ 1/n_a)

d <- data.frame(elements_measured, LOD, errors(LOD))

# Specify the file name and path
file_name <- "LOD.csv"

# Write the data frame to a CSV file
write.csv(d, file = file_name, row.names = FALSE)


# calcolo error


