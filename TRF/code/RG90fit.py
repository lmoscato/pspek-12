# import numpy as np
# from scipy.optimize import curve_fit
# import matplotlib.pyplot as plt
# import pandas as pd

# df = pd.read_csv('TRF/data/day_two/r6g_TRPL_EXC_355nm_20MHz_Slit_10%_EM_575nm_SLit_27nm_Pol_90_Att_100_100s/Custom_20231128_1621_0.csv',delimiter='\t')


# print(df)
# plt.plot(df['Time'],df['Intensity'])
# plt.show()

# # Define the fitting function
# def fitting_function(x, a, b):
#     return a * np.exp(b * x) 

# max =df['Intensity'].idxmax()
# # Skip the first 40 rows
# x_data = df['Time'][max:]
# y_data = df['Intensity'][max:]


# initial_guess = (3000, -0.14)  # Initial guess for parameters (a, b, c, d)
# fit_params, covariance = curve_fit(fitting_function, x_data, y_data, p0=initial_guess)



# # # Display the fitted parameters
# print(f"Fitted Parameters: a={fit_params[0]}, b={fit_params[1]}")

# # # Plot the results
# plt.scatter(x_data, y_data, label='Data')
# plt.plot(x_data, fitting_function(x_data, *fit_params), 'r-', label='Fitted Curve')
# plt.title('Curve Fitting Example')
# plt.xlabel('x')
# plt.ylabel('y')
# plt.legend()
# plt.show()

# b=-0.07118070188532752

tau=1/0.07118070188532752
print(tau)
