% DPPH for field correction

%data loading

[B_exp, I_exp, par] = eprload('data/dpph');

B_exp = B_exp / 10; % Conversion to mT

freq = par.MF * 1e9; % Find Frequency
h = Constant.Planck; % Define planck Constant
mu = Constant.BohrMagneton; % Define Bohr Magneton

center_idx = find(I_exp == max(I_exp) | I_exp == min(I_exp));% Find indexes of max and minima
center_idx = round(mean(center_idx)); % find index of center from in between max and minima

center = B_exp(center_idx); % find center using index of center found before 

box off
plot(B_exp, I_exp)
xlim([B_exp(1) B_exp(end)])
xline(center);

fontname("CMU Serif")



g_exp = (h * freq) / (center * 1e-3 * mu); % Calculate g value experimental
g_lit = 2.0036; %lit value of dpph g factor
center_theory = ((h * freq) / (g_lit * mu)) * 1e3;


deviation = center - center_theory;

print('plots/dpph', '-dpng');

