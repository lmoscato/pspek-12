
import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

# Define the fitting function (cubic)
def cubic_function(x, a, b, c, d):
    return a * x**3 + b * x**2 + c * x + d

# Data points
x_data = np.array([0.0, 0.1, 0.2, 0.4, 0.6, 0.8])
y_data = np.array([0.000000, 0.164238, 0.311644, 0.472109, 0.547816, 0.57745])

# Fit the data to the cubic function
fit_params, covariance = curve_fit(cubic_function, x_data, y_data)

# Plot the results with data points above the fitting line
x_smooth = np.linspace(min(x_data), max(x_data), 100)
y_smooth = cubic_function(x_smooth, *fit_params)

# Plot the fitting curve first with a lower zorder
plt.plot(x_smooth, y_smooth, 'black', linestyle='dotted', label='Fitted Cubic Curve', linewidth=1.5, zorder=1)

# Plot the data points on top with a higher zorder
plt.scatter(x_data, y_data, label='Data Points', color='black', s=18, marker='o', edgecolors='black', zorder=2)

# Customize the plot
plt.xlabel('Total acceptor concentration / %')
plt.ylabel('Acceptor quantum yield / a.u.')
plt.legend()
plt.show()

# Save the plot as a PDF
plt.savefig('QY.pdf', format='pdf')

# Display the fitted parameters
print(f"Fitted Parameters: a={fit_params[0]}, b={fit_params[1]}, c={fit_params[2]}, d={fit_params[3]}")
