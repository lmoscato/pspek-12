%SP optimal EPR spectrum

clear all; close all;

[B_exp, I_exp, par] = eprload('data/SP_optimal');
B_exp = B_exp / 10; % Convert xaxis to mT

hold on, box off;

plot(B_exp,I_exp)
xlabel('Magnetic Field [mT]');
ylabel('Intensity a.u.');
xlim([B_exp(1) B_exp(end)]);
fontname('CMU Serif');

print('plots/SP_optimal', '-dpng');

