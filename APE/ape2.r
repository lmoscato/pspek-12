# WAV file generation
rm(list = ls())

library(tuneR)
library(httpgd)

cur_dir <- getwd()
if (!grepl("/APE", cur_dir, ignore.case = TRUE)) {
    setwd("./APE")
}

source("APECPVutils.R")

filename <- "veryslowsweep"

sampling_rate <- 44100 # [Hz]
dt <- 1 / sampling_rate # dwell time [s]

t <- seq(0, 10, by = dt)

if(FALSE){ # Cosinus
    s <- .9 * cos(2*pi*t*0)
}

if(FALSE){ # Pulse
    tp <- 40e-6 #pulse duration
    s[t >= tp] <- 0  # set signal above 0.01 seconds as 0
}

if(TRUE){
    fstart <- 2000 # [Hz]
    fstop <- 4000 # [Hz]
    k <- (fstop - fstart) / max(t) # Sweep rate [Hz / s]
    s <-  sin(2*pi*(fstart + 0.5*k*t)*t)
}


s <- 0.99 * s / max(abs(s)) # normierung
# plot(t,s, type = "o", xlim=c(0.5, 0.501),
# xlab=expression(italic(t)*"/s"), 
# ylab=expression(italic(s)*"/V"))


plot(t,s,
    type = "l",
    xlim = c(),
    xlab=expression(italic(t)*"/s"), 
    ylab=expression(italic(s)*"/V")
)

WAV.filename <- sprintf("audio/%s.wav", filename)

write.WAV(WAV.filename, s, sampling_rate, 16) # writing WAV filename


#pdf

pdf.filename <- sprintf("plots/%s.pdf", filename)
dev.copy2pdf(file=pdf.filename, width=6, height=5)