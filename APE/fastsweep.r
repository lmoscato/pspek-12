#WAV-audiofile lesen
rm(list=ls())
library(tuneR)
library(httpgd)
cur_dir <- getwd()

if (!grepl("/APE", cur_dir, ignore.case = TRUE)) {
    setwd("./APE")
}

source("APECPVutils.R")

filename <- "fastsweep_rec"
WAV.filename <- sprintf("recordings/%s.wav", filename)

WAV <- read.WAV(WAV.filename)

sr <- WAV$Rate # [Hz]
dt <- 1 / sr # dwell time [s]

t <- WAV$Time
s <- WAV$Signal

t <-t - trigger(t,s, 0.1)
# windows() per aprire finestra

plot(t,s, type = "l", xlim=c(-0, .2),
xlab=expression(italic(t)*"/s"), 
ylab=expression(italic(s)*"/V"))







#pdf

pdf.filename <- sprintf("plots/%s.pdf", filename)
dev.copy2pdf(file=pdf.filename, width=6, height=5)

