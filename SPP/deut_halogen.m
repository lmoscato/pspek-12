% Deuterium /Halogen lamp comparison

clear variables, close all;

data_deut = importdata("data\D_lamp_USB4F034531_1.txt");
data_halo = importdata("data\H_lamp_USB4F034531_1.txt");

wavelength_h = data_halo.data(:,1);
intensity_h = data_halo.data(:,2);
wavelength_d = data_deut.data(:,1);
intensity_d = data_deut.data(:,2);


hold on, box off;
colororder("gem");
plot(wavelength_d, intensity_d)
plot(wavelength_h, intensity_h)

xlabel('Wavelength [nm]')
ylabel('Intensity [cps]')
fontname('CMU Serif')
legend('Deuterium lamp', 'Halogen lamp','Location','northwest')

print('plots/deut_halo_lamps', '-dpng')
