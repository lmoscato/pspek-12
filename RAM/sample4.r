# Clear the workspace
rm(list = ls())

# Load necessary libraries
library(readr)

# Read background and sample data
background <- read.delim("~/Luca_Dalia/Sample4BGE_LM_DES_HR4C23081_13-48-24-702.txt", header = FALSE, stringsAsFactors=FALSE, skip = 15)
sample <- read.delim("~/Luca_Dalia/Sample4_LM_DES_HR4C23081_13-22-45-873.txt", header = FALSE, stringsAsFactors=FALSE, skip = 15)

# Calculate wavenumbers and intensities
wavenum_auto <- 1/sample$V1*10^7
I_bg <- background$V2
I_samp <- sample$V2
I_corr <- I_samp - I_bg
max_bg_index <- which.max(I_bg)
wavenum <- -wavenum_auto + (1/532.639*10^7)

# Plot the corrected intensity against wavenumbers
plot(wavenum, I_corr, type = "l", xlim = c(4000, -1500), xaxs = "i", 
     xlab = expression("Wavenumber / cm"^(-1)), ylab = "Intensity", ylim = c(-50, 9000))

# Extract peak around 3000
peak_wavenum <- wavenum[3500:1700]
peak_I <- I_corr[3500:1700]
 
b <- seq(wavenum[3500], wavenum[1700], length = 1800)

# Plot the p
plot(peak_wavenum, peak_I, type = "p")
#lines(b,  520 * exp(-0.5 * ((b - -2800)^2) / 400^2)  )

# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) , 
            start = list(height = 520, position = 2800, width = 400), trace = TRUE)

lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) , 
             start = list(height = 520, position = 2800, width = 400), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))


##############################################

# Extract peak around 780
peak_wavenum <- wavenum[1260:1300]
peak_I <- I_corr[1260:1300]

b <- seq(wavenum[1260], wavenum[1300], length = 1800)
 
# Plot the p
plot(peak_wavenum, peak_I, type = "p")
lines(b,  1000 * exp(-0.5 * ((b - 785)^2) / 6^2)  )
 
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) , 
            start = list(height = 1000, position = 785, width = 6), trace = TRUE)

lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) , 
               start = list(height = 1000, position = 785, width = 6), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))

# Generate sequences for plotting curves
a <- seq(wavenum[1260], wavenum[1300], length = 1800)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")
 
# Plot the curves
 lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")


#####################################################################



# # Extract peak around -600
peak_wavenum <- wavenum[605:565]
peak_I <- I_corr[605:565]

b <- seq(wavenum[605], wavenum[565], length = 1000)

# Plot the p
plot(peak_wavenum, peak_I, type = "p",xlab=expression("Wavenumber / cm"^(-1)), ylab="Intensity")
#lines(b,  240 * exp(-0.5 * ((b - -670)^2) / 7^2)  )

# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) , 
             start = list(height = 240, position = -670, width = 7), trace = TRUE)
  
lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) , 
             start = list(height = 240, position = -670, width = 7), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))
#  
# # # Generate sequences for plotting curves
 a <- seq(wavenum[605], wavenum[565], length = 1000)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")
# 
# Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")
 

###########################################################


#Extract peak around 400
peak_wavenum <- wavenum[1100:1040]
peak_I <- I_corr[1100:1040]
b <- seq(wavenum[1100], wavenum[1040], length = 1800)

# Plot the p
plot(peak_wavenum, peak_I, type = "p")
#lines(b,  1700 * exp(-0.5 * ((b - 680)^2) / 7^2) +100 )
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) +100 ,
             start = list(height = 500, position = 350, width = 7), trace = TRUE)
lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) +100 ,
              start = list(height = 500, position = 350, width = 7), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))

# Generate sequences for plotting curves
a <-  seq(wavenum[1100], wavenum[1040], length = 1800)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")

# Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")
###########################################################################



#Extract peak around 600
peak_wavenum <- wavenum[1250:1175]
peak_I <- I_corr[1250:1175]
b <- seq(wavenum[1250], wavenum[1175], length = 1800)

# Plot the p
plot(peak_wavenum, peak_I, type = "p")
#lines(b,  1700 * exp(-0.5 * ((b - 680)^2) / 7^2) +100 )
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) +100 ,
             start = list(height = 8000, position = 640, width = 7), trace = TRUE)
lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) +100 ,
              start = list(height = 8000, position = 640, width = 7), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))

# Generate sequences for plotting curves
a <-  seq(wavenum[1250], wavenum[1175], length = 1800)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")

# Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")
###########################################







######################################


#
#Extract peak around200
peak_wavenum <- wavenum[1000:900]
peak_I <- I_corr[1000:900]
b <- seq(wavenum[1000], wavenum[900], length = 1800)
# Plot the p
plot(peak_wavenum, peak_I, type = "p")
#lines(b,  1000 * exp(-0.5 * ((b - 260)^2) / 7^2) +100 )
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) ,
             start = list(height = 5000, position = 160, width = 7), trace = TRUE)

lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) ,
              start = list(height = 5000, position = 160, width = 7), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))

# Generate sequences for plotting curves
a <-  seq(wavenum[1000], wavenum[900], length = 1800)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")

# # Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")
###############################################################

#  
# # # Generate sequences for plotting curves
a <- seq(wavenum[600], wavenum[560], length = 1000)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")
# 
# Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")




###########################
# Plot the corrected intensity against wavenumbers
#plot(wavenum, I_corr, type = "l", ylim = c(0, max(I_corr)), xlim = c(4000, -1000), 
     #xaxs = "i", xlab = expression("Wavenumber / cm"^(-1)), ylab = "Intensity")
# Plot the corrected intensity against wavenumbers
plot(wavenum, I_corr, type = "l", xlim = c(4000, -1500), xaxs = "i", 
     xlab = expression("Wavenumber / cm"^(-1)), ylab = "Intensity", ylim = c(-50, 9000))

# Add text annotations
text(2820, 1750, srt = 90, labels = expression("2820.05 cm"^(-1)), cex = 0.8, col = "royalblue")
text(600, 7850, srt = 90, labels = expression("640.73 cm"^(-1)), cex = 0.8, col = "royalblue")
text(787, 1900, srt = 90, labels = expression("784.73 cm"^(-1)), cex = 0.8, col = "royalblue")
text(590, 12000, srt = 90, labels = expression("670 cm"^(-1)), cex = 0.8, col = "royalblue")
text(-100, 6900, srt = 90, labels = expression("Rayleigh scattering"), cex = 0.8, col = "black")
text(-660, 1200, srt = 90, labels = expression("-670.97 cm"^(-1)), cex = 0.8, col = "royalblue")
text(130, 5542, srt = 90, labels = expression("168.30 cm"^(-1)), cex = 0.8, col = "royalblue")
text(351.35, 1500, srt = 90, labels = expression("351.35 cm"^(-1)), cex = 0.8, col = "royalblue")

