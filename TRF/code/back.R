rm(list = ls()) 

##Background data

DC <- 39.05849#Darkcounts

#TRPL donor background 
infilename0 <- "~/Desktop/Team 12/day_two/0_TRPL_EXC_355nm_20MHz_Slit_0.1%_EM_480nm_SLit_27nm_Att_100_100s/Custom_20231128_1552_0.dat"
#C:C:/UsersC:/Users/dalia/Documents/Desktop/Team 12/day_two/0_TRPL_EXC_355nm_20MHz_Slit_0.1%_EM_480nm_SLit_27nm_Att_100_100s/Custom_20231128_1552_0.dat
PL0 <- read.table(infilename0, skip = 1020)
Int0 <- (PL0[ ,2])
Int0 <- mean(Int0) #Mean

infilename1 <- "~/Desktop/Team 12/day_two/0.1_TRPL_EXC_355nm_20MHz_Slit_0.1%_EM_480nm_SLit_27nm_Att_100_100s/Custom_20231128_1354_0.dat"
PL1 <- read.table(infilename1, skip = 1020)
Int1 <- (PL1[ ,2])
Int1 <- mean(Int1) #Mean

infilename2 <- "~/Desktop/Team 12/day_two/0.2_TRPL_EXC_355nm_20MHz_Slit_0.1%_EM_480nm_SLit_27nm_Att_100_100s/Custom_20231128_1419_0.dat"
PL2 <- read.table(infilename2, skip = 1020)
Int2 <- (PL2[ ,2])
Int2 <- mean(Int2) #Mean

infilename4 <- "~/Desktop/Team 12/day_two/0.4_TRPL_EXC_355nm_20MHz_Slit_0.1%_EM_480nm_SLit_27nm_Att_100_100s/Custom_20231128_1455_0.dat"
PL4 <- read.table(infilename4, skip = 1020)
Int4 <- (PL4[ ,2])
Int4 <- mean(Int4) #Mean

infilename6 <- "~/Desktop/Team 12/day_two/0.6_TRPL_EXC_355nm_20MHz_Slit_0.1%_EM_480nm_SLit_27nm_Att_100_100s/Custom_20231128_1518_0.dat"
PL6 <- read.table(infilename6, skip = 1020)
Int6 <- (PL6[ ,2])
Int6 <- mean(Int6) #Mean

infilename8 <- "~/Desktop/Team 12/day_two/0.8_TRPL_EXC_355nm_20MHz_Slit_0.1%_EM_480nm_SLit_27nm_Att_100_100s/Custom_20231128_1534_0.dat"
PL8 <- read.table(infilename8, skip = 1020)
Int8 <- (PL8[ ,2] )
Int8 <- mean(Int8) #Mean

#-------------------------------------------------------------------------------
#TRPL acceptor background

infilename12 <- "~/Desktop/Team 12/day_two/0.1_TRPL_EXC_355nm_20MHz_Slit_0.1%_EM_610nm_SLit_27nm_Att_15_100s/Custom_20231128_1358_0.dat"
PL12 <- read.table(infilename12, skip = 1020)
Int12 <- (PL12[ ,2] - DC)
Int12 <- mean(Int12)

infilename22 <- "~/Desktop/Team 12/day_two/0.2_TRPL_EXC_355nm_20MHz_Slit_0.1%_EM_610nm_SLit_27nm_Att_15_100s/Custom_20231128_1422_0.dat"
PL22 <- read.table(infilename22, skip = 1020)

Int22 <- (PL22[ ,2] - DC)
Int22 <- mean(Int22)

#infilename42 <- "Day2/Ex2/FL03_LR04_TRPL_acceptor_604/TRPL04.dat"
#PL42 <- read.table(infilename42, skip = 1020)

#Int42 <- (PL42[ ,2] - DC)
#Int42 <- mean(Int42)

infilename62 <- "~/Desktop/Team 12/day_two/0.6_TRPL_EXC_355nm_20MHz_Slit_0.1%_EM_610nm_SLit_27nm_Att_15_100s/Custom_20231128_1522_0.dat"
PL62 <- read.table(infilename62, skip = 1020)

Int62 <- (PL62[ ,2] - DC)
Int62 <- mean(Int62)

infilename82 <- "~/Desktop/Team 12/day_two/0.8_TRPL_EXC_355nm_20MHz_Slit_0.1%_EM_610nm_SLit_27nm_Att_15_100s/Custom_20231128_1537_0.dat"
PL82 <- read.table(infilename82, skip = 1020)

Int82 <- (PL82[ ,2] - DC)
Int82 <- mean(Int82)

#-----------------------------------------------------------------------
#Anisotropy background
infilename00 <- "~/Desktop/Team 12/day_two/r6g_TRPL_EXC_355nm_20MHz_Slit_10%_EM_575nm_SLit_27nm_Pol_0_Att_100_100s/Custom_20231128_1618_0.dat"
PL00 <- read.table(infilename00, skip = 1020)
Int00 <- (PL00[ ,2] - DC)
Int00 <- mean(Int00) #Mean

infilename90 <- "~/Desktop/Team 12/day_two/r6g_TRPL_EXC_355nm_20MHz_Slit_10%_EM_575nm_SLit_27nm_Pol_90_Att_100_100s/Custom_20231128_1621_0.dat"
PL90 <- read.table(infilename90, skip = 1020)
Int90 <- (PL90[ ,2] - DC)
Int90 <- mean(Int90) #Mean

#Printing of the result
print("Background TRPL donor: ")
print(Int0)
print(Int1)
print(Int2)
print(Int4)
print(Int6)
print(Int8)

print("Background TRPL acceptor: ")
print(Int12)
print(Int22)
#print(Int42)
print(Int62)
print(Int82)

print("Background Anisotropy")
print(Int00)
print(Int90)



