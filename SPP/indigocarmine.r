#B----------------------------------------------------------------
rm( list =ls ())
library(colorspace)
library(httpgd)

cur_dir <- getwd()
if (!grepl("/SPP", cur_dir, ignore.case = TRUE)) {
    setwd("./SPP")
}

solc0 <-read.table("data/stock_Absorbance_01.txt", header = F, skip = 16)
sol2 <- read.table("data/stock_Absorbance_02.txt", header = F, skip = 16)
sol3 <- read.table("data/stock_Absorbance_03.txt", header = F, skip = 16)
sol4 <- read.table("data/stock_Absorbance_04.txt", header = F, skip = 16)
sol5 <- read.table("data/stock_Absorbance_05.txt", header = F, skip = 16)
sol6 <- read.table("data/stock_Absorbance_06.txt", header = F, skip = 16)
sol7 <- read.table("data/stock_Absorbance_07.txt", header = F, skip = 16)
sol8 <- read.table("data/stock_Absorbance_08.txt", header = F, skip = 16)
sol9 <- read.table("data/stock_Absorbance_09.txt", header = F, skip = 16)
sol10 <-read.table("data/stock_Absorbance_10.txt", header = F, skip = 16)
sol11<- read.table("data/stock_Absorbance_11.txt", header = F, skip = 16)
sol12<- read.table("data/stock_Absorbance_12.txt", header = F, skip = 16)


abs <- c(289, 610) #absorbance of indigocarmine
idx <- c(336, 520, 2112) #indexes of wavelengths in table

m_stock <- c(3, 2.228, 1.731, 1.918, 1.745, 1.944, 1.641, 1.519, 1.635, 1.934, 0.975, 0.3032)
m_water <- c(0, 0.806, 2.592, 3.553, 4.646, 5.336, 7.397, 8.946, 10.335, 12.569, 11.784, 9.449)

c <- m_stock / (m_stock + m_water)

absorbance1 <- c(solc0 [idx[1], 2], sol2[idx[1], 2], sol3[idx[1], 2],
sol4[idx[1], 2], sol5[idx[1], 2], sol6[idx[1], 2], sol7[idx[1], 2], sol8[idx[1], 2], sol9[idx[1] ,2], sol10[idx[1] ,2],
sol11[idx[1] ,2], sol12[idx[1] ,2])

absorbance2 <- c(solc0[idx[2], 2], sol2[idx[2], 2], sol3[idx[2], 2],
sol4[idx[2], 2], sol5[idx[2], 2], sol6[idx[2], 2], sol7[idx[2], 2], sol8[idx[2], 2], sol9[idx [2],2],
sol10[idx [2],2], sol11[idx [2],2], sol12[idx [2],2])

absorbance3 <- c(solc0[idx[3], 2], sol2[idx[3], 2], sol3[idx[3], 2],
sol4[idx[3], 2], sol5[idx[3], 2], sol6[idx[3], 2], sol7[idx[3], 2], sol8[idx[3], 2], sol9[idx[3],2],
sol10[idx[3],2], sol11[idx[3],2], sol12[idx[3],2])

model_falselight1 <- nls(10^ absorbance1 ~ (1 + delta )/(10^( -s*c)+ delta ),
start = list (s=7.1 , delta =0.000776), trace =F)
model_falselight2 <- nls(10^ absorbance2 ~ (1 + delta )/(10^( -s*c)+ delta ),
start = list (s=7.1 , delta =0.000776), trace =F)
model_falselight3 <- nls(10^ absorbance3 ~ (1 + delta )/(10^( -s*c)+ delta ),
start = list (s=7.1 , delta =0.000776), trace =F)

s1 <- summary(model_falselight1)$param[1 ,1]
s1.ci <- summary(model_falselight1)$param[1 ,2] * qt(0.975, df = df.residual(model_falselight1))

delta1 <- summary(model_falselight1)$param[2 ,1]
delta1.ci <- summary(model_falselight1)$param[2 ,2] * qt (0.975, df=df.residual(model_falselight1))

s2 <- summary(model_falselight2)$param[1 ,1]
s2.ci <- summary(model_falselight2)$param[1 ,2] * qt (0.975, df=df.residual(model_falselight2))

delta2 <- summary(model_falselight2)$param[2 ,1]
delta2.ci <- summary(model_falselight2)$param[2 ,2] * qt (0.975 , df = df.residual(model_falselight2))

s3 <- summary(model_falselight3)$param[1 ,1]
s3.ci <- summary(model_falselight3)$param[1 ,2] * qt (0.975, df=df.residual(model_falselight3))

delta3 <- summary(model_falselight3)$param[2 ,1]
delta3.ci <- summary(model_falselight3)$param[2 ,2] * qt (0.975 , df = df.residual(model_falselight3))

# plotting

if (1) {
windows(height= 7, width = 9)
par(mar= c(5, 6, 3, 2) + 0.1)
lwd <- 1.5

col <- qualitative_hcl(12, palette = "Dark")
plot (solc0,
    type ="l",
    lwd =2,
    las =1,
    ann =F,
    ylim = c(0 ,3.4),
    xlim = c(190 ,750)
)

lines(solc0, lwd = lwd, col = col[1])
lines(sol2, lwd = lwd, col = col[2])
lines(sol3, lwd = lwd, col = col[3])
lines(sol4, lwd = lwd, col = col[4])
lines(sol5, lwd = lwd, col = col[5])
lines(sol6, lwd = lwd, col = col[6])
lines(sol7, lwd = lwd, col = col[7])
lines(sol8, lwd = lwd, col = col[8])
lines(sol9, lwd = lwd, col = col[9])
lines(sol10, lwd = lwd , col = col[10])
lines(sol11, lwd = lwd , col = col[11])
lines(sol12, lwd = lwd , col = col[12])

mtext(side =1, text = expression(lambda ~ "/ nm"), line =3, cex =1.5)
mtext(side =2, text = "absorbance", line =4, cex =1.5)

axis (1, seq (0 ,1000, by =50), lab = NA, tck = -0.01)
axis (2, seq (-2, 10, by =0.25), lab = NA, tck = -0.01)

cols <- diverging_hcl(palette = "Berlin", 3)

points(solc0[idx ,1], solc0[idx ,2],    pch = c(21 ,22, 23), bg= "white", col = cols)
points(sol2[idx ,1],  sol2[idx ,2],     pch = c(21 ,22, 23), bg= "white", col = cols)
points(sol3[idx ,1], sol3[idx ,2],      pch = c(21 ,22, 23), bg= "white", col = cols)
points(sol4[idx ,1], sol4[idx ,2],      pch = c(21 ,22, 23), bg= "white", col = cols)
points(sol5[idx ,1], sol5[idx ,2],      pch = c(21 ,22, 23), bg= "white", col = cols)
points(sol6[idx ,1], sol6[idx ,2],      pch = c(21 ,22, 23), bg= "white", col = cols)
points(sol7[idx ,1], sol7[idx ,2],      pch = c(21 ,22, 23), bg= "white", col = cols)
points(sol8[idx ,1], sol8[idx ,2],      pch = c(21 ,22, 23), bg= "white", col = cols)
points(sol9[idx ,1], sol9[idx ,2],      pch = c(21 ,22, 23), bg= "white", col = cols)
points(sol10[idx ,1], sol10[idx ,2],    pch = c(21 ,22, 23), bg= "white", col = cols)
points(sol11[idx ,1], sol11[idx ,2],    pch = c(21 ,22, 23), bg= "white", col = cols)
points(sol12[idx ,1], sol12[idx ,2],    pch = c(21 ,22, 23), bg= "white", col = cols)

dev.copy2pdf ( file ="plots/indigocarmine_absorbance.pdf")
dev.off ()
}

if (1){

windows(height =7, width =9)
par(mar= c(5, 6, 3, 2) + 0.1)
plot(c, absorbance1,
    pch = 21,
    bg = "white",
    lwd = 1.5,
    las = 1,
    ann = F,
    ylim = c(0 ,3.4)
)

model1 <- lm(absorbance1[5:12] ~ 0 + c [5:12])
model2 <- lm(absorbance2[5:12] ~ 0 + c [5:12])
model3 <- lm(absorbance3[5:12] ~ 0 + c [5:12])

abline(model1, lty = 2, col = cols[1])
abline(model2, lty = 2, col = cols[2])
abline(model3, lty = 2, col = cols[3])
x_test <- seq (0,1, length =1000)
y_test1 <- log10(predict(model_falselight1 , list(c= x_test)))
y_test2 <- log10(predict(model_falselight2 , list(c= x_test)))
y_test3 <- log10(predict(model_falselight3 , list(c= x_test)))

lines(x_test, y_test1, lwd = 1.5, col = cols[1])
lines(x_test, y_test2, lwd = 1.5, col = cols[2])
lines(x_test, y_test3, lwd = 1.5, col = cols[3])
points(c, absorbance1, pch = 21, bg = "white", lwd = 1.5, col = cols[1])
points(c, absorbance2, pch = 22, bg = "white", lwd = 1.5, col = cols[2])
points(c, absorbance3, pch = 23, bg = "white", lwd = 1.5, col = cols[3])

mtext(side =1, text = expression ("c"~"/"~"c" [0]) , line =3, cex =1.5)
mtext(side =2, text = "absorbance", line =4, cex =1.5)

axis(1,seq(0 ,10 , by =0.1) , lab =NA , tck = -0.01)
axis(2,seq( -2 ,10 , by =0.25) , lab =NA , tck = -0.01)
axis(3,seq(0 ,10 , by =0.2) , lab =NA , tck =0.02)
axis(3,seq(0 ,10 , by =0.1) , lab =NA , tck =0.01)
axis(4,seq( -2 ,10 , by =0.25) , lab =NA , tck =0.01)
axis(4,seq( -2 ,10 , by =0.5) , lab =NA , tck =0.02)

dev.copy2pdf(file = "plots/B_abs_concentration.pdf")
dev.off ()

}