rm(list =ls())


# PACKAGES #########################
library(colorspace)
library(httpgd)
library(magicaxis)

# OTHER RULES ##################################
cur_dir <- getwd()
if (!grepl("/LIM", cur_dir, ignore.case = TRUE)) {
    setwd("./LIM")
}

par(family = "CM Roman", mfrow = c(1,1))

# DATA EXTRACTION ###########################################
low_bg <- read.table(file = "data/R_BGLOW.D/r_bglow.d#/r_bglow.tab", skip = 9, header = TRUE)
high_bg <- read.table(file =  "data/R_BGH.D/R_BGH.d#/R_BGH.tab", skip = 9, header = TRUE)

low_nist <- read.table(file = "data/R_NLOW.D/R_NLOW.d#/R_NLOW.tab", skip = 9, header = TRUE)
high_nist <- read.table(file = "data/R_NH.D/R_NH.d#/R_NH.tab", skip = 9, header = TRUE)


full_spec <- data.frame(append(low_bg[,1], high_bg[,1]), append(low_nist[,2] - low_bg[,2], high_nist[,2] - high_bg[,2]))

masses <- full_spec[,1]
intensities <- full_spec[,2]

# PLOTTING BG SPECTRUM ###########################################

plot(masses, append(low_bg[,2], high_bg[,2]),
    xaxt = "n",
    yaxt = "n",
    type = "h",
    ylab = "Intensity [CPS]",
    xlab = "Mass [Da]",
    xlim = c(8,235),
    cex.lab = 1.3
)
magaxis(2, hersh = TRUE, minorn = 9, cex.axis = 1.3)
magaxis(1, hersh = TRUE, majorn = 10, minorn = 10, cex.axis = 1.3)

# PLOTTING FULL SPECTRUM ############################################
plot(masses, intensities,
    type = "h",
    log = "y",
    xaxt = "n",
    yaxt = "n",
    ylab = "Intensity [CPS]",
    xlab = "Mass [Da]",
    xlim = c(8, 235),
    cex.lab = 1.3
)
magaxis(2, hersh = TRUE, minorn=9, cex.axis = 1.3)
magaxis(1, hersh = TRUE, majorn = 10, minorn = 10, cex.axis = 1.3)



# FORMATTING PEAKS IMAGE ###############################################


# Set up the layout matrix
layout_matrix <- matrix(c(1, 1, 2, 2, 3, 3, 6, 4, 4, 5, 5, 6), nrow = 2, byrow = TRUE)

# Define the heights of the rows
layout_heights <- c(1, 1)

# Set the layout
layout(layout_matrix, heights = layout_heights, widths = c())


# EXTRACTING LITHIUM PEAK ############################################
# par(mfrow = c(2,3))
mass <- 7
idx <- (which(masses == 7)-10):(which(masses==7)+10) # take +- 0.5 mass around correct mass of Li

peak_m <- masses[idx]
peak_cps <- intensities[idx]

xseq <- seq(6.5, 7.5, length = 1000)

gauss <- nls(peak_cps ~ height * exp(-0.5 * ((peak_m - position)^2) / width^2) ,
            start = list(height = 122000, position = 7, width = 0.4), trace = FALSE)

lorenz <- nls(peak_cps ~ height * (width^2 / ((peak_m - position)^2 + width^2)),
             start = list(height = 122000, position = 7, width = 0.5), trace = FALSE)

curve_g <- predict(gauss, list(peak_m = xseq), interval = "confidence")
curve_l <- predict(lorenz, list(peak_m = xseq), interval = "confidence")

# Calculate the 10% height threshold
ten_percent_height <- 0.1 * coef(gauss)["height"]

# Find the indices where the curve crosses the 10% threshold
idx_threshold <- which(curve_g >= ten_percent_height)
idx_threshold <- c(idx_threshold[1], tail(idx_threshold, 1))

# Calculate the width at 10% of the height
width_10_percent <- abs(diff(range(xseq[idx_threshold])))

resolutions <- mass / width_10_percent


plot(peak_m, peak_cps,
    xaxt = "n",
    yaxt = "n",
    ylab = "Intensity [CPS]",
    xlab = "Mass [Da]",
    ylim = c(0, 1.5e5)
)
magaxis(2, hersh = TRUE, minorn = 9)
magaxis(1, hersh = TRUE, minorn = 5)

segments(xseq[idx_threshold[1]], x1 = xseq[idx_threshold[2]], y0 = curve_g[idx_threshold[1]], lwd = 1)
abline(v = coef(gauss)["position"], lty = 2)

lines(xseq, curve_g,
    col = "red"
)

# lines(xseq, curve_l,
    # col = "blue"
# )

# EXTRACTING NA PEAK #######################################################

mass <- 23
idx_c <- which(masses == mass)
idx <- (idx_c - 10):(idx_c + 10)


peak_m <- masses[idx]
peak_cps <- intensities[idx]

xseq <- seq(mass - 0.5, mass + 0.5, length = 1000)

gauss <- nls(peak_cps ~ height * exp(-0.5 * ((peak_m - position)^2) / width^2) ,
            start = list(height = intensities[idx_c], position = mass, width = 0.4), trace = FALSE)

lorenz <- nls(peak_cps ~ height * (width^2 / ((peak_m - position)^2 + width^2)),
             start = list(height = intensities[idx_c], position = mass, width = 0.5), trace = FALSE)

curve_g <- predict(gauss, list(peak_m = xseq), interval = "confidence")
curve_l <- predict(lorenz, list(peak_m = xseq), interval = "confidence")


# Calculate the 10% height threshold
ten_percent_height <- 0.1 * coef(gauss)["height"]

# Find the indices where the curve crosses the 10% threshold
idx_threshold <- which(curve_g >= ten_percent_height)
idx_threshold <- c(idx_threshold[1], tail(idx_threshold, 1))

# Calculate the width at 10% of the height
width_10_percent <- abs(diff(range(xseq[idx_threshold])))

resolutions <- c(resolutions, mass / width_10_percent)


plot(peak_m, peak_cps,
    xaxt = "n",
    yaxt = "n",
    ylab = "Intensity [CPS]",
    xlab = "Mass [Da]",
    ylim = c(0, max(curve_g))
)
magaxis(2, hersh = TRUE, minorn = 9)
magaxis(1, hersh = TRUE, minorn = 5)


segments(xseq[idx_threshold[1]], x1 = xseq[idx_threshold[2]], y0 = curve_g[idx_threshold[1]], lwd = 1)
abline(v = coef(gauss)["position"], lty = 2)


lines(xseq, curve_g,
    col = "red"
)

# lines(xseq, curve_l,
    # col = "blue"
# )

# EXTRACTING Y PEAK #######################################################

mass <- 89
idx_c <- which(masses == mass)
idx <- (idx_c - 10):(idx_c + 10)


peak_m <- masses[idx]
peak_cps <- intensities[idx]

xseq <- seq(mass - 0.5, mass + 0.5, length = 1000)

gauss <- nls(peak_cps ~ height * exp(-0.5 * ((peak_m - position)^2) / width^2) ,
            start = list(height = intensities[idx_c], position = mass, width = 0.4), trace = FALSE)

lorenz <- nls(peak_cps ~ height * (width^2 / ((peak_m - position)^2 + width^2)),
             start = list(height = intensities[idx_c], position = mass, width = 0.5), trace = FALSE)

curve_g <- predict(gauss, list(peak_m = xseq), interval = "confidence")
curve_l <- predict(lorenz, list(peak_m = xseq), interval = "confidence")

# Calculate the 10% height threshold
ten_percent_height <- 0.1 * coef(gauss)["height"]

resolutions <- c(resolutions, mass / width_10_percent)

# Find the indices where the curve crosses the 10% threshold
idx_threshold <- which(curve_g >= ten_percent_height)
idx_threshold <- c(idx_threshold[1], tail(idx_threshold, 1))

# Calculate the width at 10% of the height
width_10_percent <- abs(diff(range(xseq[idx_threshold])))



plot(peak_m, peak_cps,
    xaxt = "n",
    yaxt = "n",
    ylab = "Intensity [CPS]",
    xlab = "Mass [Da]",
    ylim = c(0, max(c(max(curve_l),max(curve_g))))
)
magaxis(2, hersh = TRUE, minorn = 9)
magaxis(1, hersh = TRUE, minorn = 5)


segments(xseq[idx_threshold[1]], x1 = xseq[idx_threshold[2]], y0 = curve_g[idx_threshold[1]], lwd = 1)
abline(v = coef(gauss)["position"], lty = 2)


lines(xseq, curve_g,
    col = "red"
)

# lines(xseq, curve_l,
    # col = "blue"
# )


# SPACER #############################3


# EXTRACTING CE PEAK #####################################

mass <- 140
idx_c <- which(masses == mass)
idx <- (idx_c - 10):(idx_c + 10)


peak_m <- masses[idx]
peak_cps <- intensities[idx]

xseq <- seq(mass - 0.5, mass + 0.5, length = 1000)

gauss <- nls(peak_cps ~ height * exp(-0.5 * ((peak_m - position)^2) / width^2) ,
            start = list(height = intensities[idx_c], position = mass, width = 0.4), trace = FALSE)

lorenz <- nls(peak_cps ~ height * (width^2 / ((peak_m - position)^2 + width^2)),
             start = list(height = intensities[idx_c], position = mass, width = 0.5), trace = FALSE)

curve_g <- predict(gauss, list(peak_m = xseq), interval = "confidence")
curve_l <- predict(lorenz, list(peak_m = xseq), interval = "confidence")

# Calculate the 10% height threshold
ten_percent_height <- 0.1 * coef(gauss)["height"]

resolutions <- c(resolutions, mass / width_10_percent)

# Find the indices where the curve crosses the 10% threshold
idx_threshold <- which(curve_g >= ten_percent_height)
idx_threshold <- c(idx_threshold[1], tail(idx_threshold, 1))

# Calculate the width at 10% of the height
width_10_percent <- abs(diff(range(xseq[idx_threshold])))



plot(peak_m, peak_cps,
    xaxt = "n",
    yaxt = "n",
    ylab = "Intensity [CPS]",
    xlab = "Mass [Da]",
    ylim = c(0, max(curve_g))
)
magaxis(2, hersh = TRUE, minorn = 9)
magaxis(1, hersh = TRUE, minorn = 5)


segments(xseq[idx_threshold[1]], x1 = xseq[idx_threshold[2]], y0 = curve_g[idx_threshold[1]], lwd = 1)
abline(v = coef(gauss)["position"], lty = 2)


lines(xseq, curve_g,
    col = "red"
)

# lines(xseq, curve_l,
    # col = "blue"
# )


# EXTRACTING U PEAK #######################################################

mass <- 238
idx_c <- which(masses == mass)
idx <- (idx_c - 7):(idx_c + 9)


peak_m <- masses[idx]
peak_cps <- intensities[idx]

xseq <- seq(mass - 0.5, mass + 0.5, length = 1000)

gauss <- nls(peak_cps ~ height * exp(-0.5 * ((peak_m - position)^2) / width^2) ,
            start = list(height = intensities[idx_c], position = mass, width = 0.4), trace = FALSE)

lorenz <- nls(peak_cps ~ height * (width^2 / ((peak_m - position)^2 + width^2)),
             start = list(height = intensities[idx_c], position = mass, width = 0.5), trace = FALSE)

curve_g <- predict(gauss, list(peak_m = xseq), interval = "confidence")
curve_l <- predict(lorenz, list(peak_m = xseq), interval = "confidence")


# Calculate the 10% height threshold
ten_percent_height <- 0.1 * coef(gauss)["height"]

# Find the indices where the curve crosses the 10% threshold
idx_threshold <- which(curve_g >= ten_percent_height)
idx_threshold <- c(idx_threshold[1], tail(idx_threshold, 1))

# Calculate the width at 10% of the height
width_10_percent <- abs(diff(range(xseq[idx_threshold])))

resolutions <- c(resolutions, mass / width_10_percent)

# Print the width at 10% height
# cat("Width at 10% of peak height: ", width_10_percent, "\n")


plot(peak_m, peak_cps,
    xaxt = "n",
    yaxt = "n",
    ylab = "Intensity [CPS]",
    xlab = "Mass [Da]",
    ylim = c(0, max(curve_g))
)
magaxis(2, hersh = TRUE, minorn = 5)
magaxis(1, hersh = TRUE, minorn = 5)

segments(xseq[idx_threshold[1]], x1 = xseq[idx_threshold[2]], y0 = curve_g[idx_threshold[1]], lwd = 1)
abline(v = coef(gauss)["position"], lty = 2)

lines(xseq, curve_g,
    col = "red"
)

# lines(xseq, curve_l,
    # col = "blue"
# )

# DATA OUTPUT ################################################
names(resolutions) <- c("Li", "Na", "Y", "Ce", "U")

write.csv(file = "data/resolutions.csv", data.frame(resolutions))