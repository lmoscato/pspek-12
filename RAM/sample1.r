# Clear the workspace
rm(list = ls())

# Load necessary libraries
library(readr)

# Read background and sample data
background <- read.delim("~/Nico_Martim/NicoMartim Background _HR4D20551_16-54-16-280.txt", header = FALSE, stringsAsFactors=FALSE, skip = 15)
sample <- read.delim("~/Nico_Martim/NicoMartim Sample I _HR4D20551_16-49-23-212.txt ", header = FALSE, stringsAsFactors=FALSE, skip = 15)

# Calculate wavenumbers and intensities
wavenum_auto <- 1/sample$V1*10^7
I_bg <- background$V2
I_samp <- sample$V2
I_corr <- I_samp - I_bg
max_bg_index <- which.max(I_bg)
wavenum <- -wavenum_auto + (1/532.639*10^7)

# Plot the corrected intensity against wavenumbers
plot(wavenum, I_corr, type = "l", xlim = c(4000, -1500), xaxs = "i", 
     xlab = expression("Wavenumber / cm"^(-1)), ylab = "Intensity", ylim = c(-50, 2000))


 #Extract peak around 2900
peak_wavenum <- wavenum[2700:2630]
peak_I <- I_corr[2700:2630]
b <- seq(wavenum[2700], wavenum[2630], length = 1800)

# Plot the p
plot(peak_wavenum, peak_I, type = "p")
lines(b,  800 * exp(-0.5 * ((b - 2960)^2) / 7^2)  )
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) +50 ,
             start = list(height = 800, position = 2960, width = 7), trace = TRUE)
lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2))+50 ,
                start = list(height = 800, position = 2960, width = 7), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))

# Generate sequences for plotting curves
a <- seq(wavenum[2700], wavenum[2630], length = 1800)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")
# Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")



###################################################################

 #Extract peak around 700
peak_wavenum <- wavenum[1260:1215]
peak_I <- I_corr[1260:1215]
b <- seq(wavenum[1260], wavenum[1215], length = 1800)

# Plot the p
plot(peak_wavenum, peak_I, type = "p")
lines(b,  1700 * exp(-0.5 * ((b - 680)^2) / 7^2) +100 )
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) +100 ,
              start = list(height = 1700, position = 680, width = 7), trace = TRUE)
lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) +100 ,
             start = list(height = 1700, position = 680, width = 7), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))

# Generate sequences for plotting curves
 a <-  seq(wavenum[1260], wavenum[1215], length = 1800)
 curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
 curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")

 # Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")
###########################################################################


#
#Extract peak around200
peak_wavenum <- wavenum[1080:1000]
peak_I <- I_corr[1080:1000]
b <- seq(wavenum[1080], wavenum[1000], length = 1800)
# Plot the p
plot(peak_wavenum, peak_I, type = "p")
lines(b,  1000 * exp(-0.5 * ((b - 260)^2) / 7^2) +100 )
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) +100 ,
              start = list(height = 1000, position = 260, width = 7), trace = TRUE)

lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) +100 ,
             start = list(height = 1000, position = 260, width = 7), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))

# Generate sequences for plotting curves
a <-  seq(wavenum[1080], wavenum[1000], length = 1800)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")

# # Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")
###############################################################




#Extract peak around 1400
peak_wavenum <- wavenum[1700:1550]
peak_I <- I_corr[1700:1550]
b <- seq(wavenum[1700], wavenum[1550], length = 1800)

# Plot the p 
plot(peak_wavenum, peak_I, type = "p")
#lines(b,  1000 * exp(-0.5 * ((b - 260)^2) / 7^2) +100 )
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) +50 ,
             start = list(height = 200, position = 1400, width = 10), trace = TRUE)

lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) +50 ,
              start = list(height = 200, position = 1400, width = 10), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))

# Generate sequences for plotting curves
a <-  seq(wavenum[1700], wavenum[1550], length = 1800)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")

# Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")

###################################################################

#Extract peak around 1140
peak_wavenum <- wavenum[1530:1410]
peak_I <- I_corr[1530:1410]
b <- seq(wavenum[1560], wavenum[1400], length = 1800)

# Plot the p 
plot(peak_wavenum, peak_I, type = "p")
#lines(b,  1000 * exp(-0.5 * ((b - 260)^2) / 7^2) +100 )
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) +50 ,
             start = list(height = 125, position = 1140, width = 5), trace = TRUE)

lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) +50 ,
              start = list(height = 125, position = 1140, width = 5), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))

# Generate sequences for plotting curves
a <-  seq(wavenum[1560], wavenum[1410], length = 1800)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")

# Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")


# Plot the corrected intensity against wavenumbers
#plot(wavenum, I_corr, type = "l", ylim = c(0, max(I_corr)), xlim = c(4000, -1000), 
#xaxs = "i", xlab = expression("Wavenumber / cm"^(-1)), ylab = "Intensity")
# Plot the corrected intensity against wavenumbers
plot(wavenum, I_corr, type = "l", xlim = c(4000, -1500), xaxs = "i", 
     xlab = expression("Wavenumber / cm"^(-1)), ylab = "Intensity", ylim = c(-50, 2000))

# Add text annotations
text(2900, 750, srt = 90, labels = expression("2965.76 cm"^(-1)), cex = 0.8, col = "royalblue")
text(1130, 350, srt = 90, labels = expression("1134.02 cm"^(-1)), cex = 0.8, col = "royalblue")
text(620, 1850, srt = 90, labels = expression("682.58 cm"^(-1)), cex = 0.8, col = "royalblue")
text(220, 1060, srt = 90, labels = expression("264.10 cm"^(-1)), cex = 0.8, col = "royalblue")
#text(-100, 5200, srt = 90, labels = expression("Rayleigh scattering"), cex = 0.8, col = "black")
text(1400, 450, srt = 90, labels = expression("1401.47 cm"^(-1)), cex = 0.8, col = "royalblue")
