% boxcar width influence on spectra

clear variables, close all;

p = dir('data/p-nitrophenol_*.txt');
N = length(p);

A = cell(1, N);

for k = 1:N
    fid = fopen(append(p(k).folder, '\', p(k).name), 'r');
    A{k} = readtable(fid);
end