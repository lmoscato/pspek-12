rm(list = ls()) 

##Dark counts calculations

#Data
infilename1 <- "~/Desktop/Team 12/day_one/darkcounts_PL_EXC_375nm_Slit_1.7nm_EM_SLit_2.7nm_Att_100_0.1s/Custom_20231127_1343_1.dat"
DC1 <- read.table(infilename1, skip = 35)
Intensity <- DC1[ ,2]

infilename2 <- "~/Desktop/Team 12/day_two/dark_PL_EXC_Slit_0.1%_EM_SLit_2.0nm_Att_100_0.1s/Custom_20231128_1600_1.dat"
DC2_20 <- read.table(infilename2, skip = 35)
Intensity20 <- DC2_20[ ,2]

infilename3 <- "~/Desktop/Team 12/day_two/dark_PL_EXC_Slit_0.1%_EM_SLit_2.5nm_Att_100_0.1s/Custom_20231128_1601_1.dat"
DC2_25 <- read.table(infilename2, skip = 35)
Intensity25 <- DC2_25[ ,2]

#Mean calculation
mDC1 <- mean(Intensity)
print(mDC1)

mDC20 <- mean(Intensity20)
print(mDC20)

mDC25 <- mean(Intensity25)
print(mDC25)