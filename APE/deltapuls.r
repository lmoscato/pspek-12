#WAV-audiofile lesen
rm(list=ls())
library(tuneR)
library(httpgd)
library(pracma)


cur_dir <- getwd()

if (!grepl("/APE", cur_dir, ignore.case = TRUE)) {
    setwd("./APE")
}

source("APECPVutils.R")

filename <- "deltapuls_rec"
WAV.filename <- sprintf("recordings/%s.wav", filename)

WAV <- read.WAV(WAV.filename)

sampling_rate <- WAV$Rate # [Hz]
dt <- 1 / sampling_rate # dwell time [s]

t <- WAV$Time
s <- WAV$Signal

t <-t - trigger(t,s, 0.1)
#windows() #per aprire finestra



# # Use locator to interactively record 7 coordinates
# click_points <- locator(7, type = "p", col = "red", pch = 16)

# # Display the recorded coordinates
# cat("Recorded Coordinates:\n")
# for (i in 1:7) {
#   cat("Point", i, ": x =", click_points$x[i], ", y =", click_points$y[i], "\n")
# }



# locator(2)$x -> p per fare locator
# diff(p) per trovare t_e
t_e <- 0.003945313
# 0.003945313 circa

#  e <- 0.75 * exp(-t/0.01)
# lines(t,e,
#     col = "red"
# )


# # Use locator to interactively record 7 coordinates
# click_points <- locator(7, type = "p", col = "red", pch = 16)

# # Display the recorded coordinates
# cat("Recorded Coordinates:\n")
# for (i in 1:7) {
#   cat("Point", i, ": x =", click_points$x[i], ", y =", click_points$y[i], "\n")
# }
coordinates <- data.frame(
x= c(7.592534e-06, 0.003993673, 0.007865865, 0.01173806, 0.01572414, 0.01953939, 0.02352547),
y= c( 0.7571761, 0.503644, 0.2610399, 0.1976569, 0.1364595, 0.08181892, 0.05777707 )
)
# Fit the exponential function
exp_fit <- nls(y ~ a * exp(x / b), data = coordinates, start = list(a = 0.7, b = -0.003945313))

# Display the summary of the fit
print(summary(exp_fit))

x_interval <- seq(0, 0.08, length.out = 1000)

# Plot the data and the fitted curve
plot(t,s, type = "l", xlim=c(-0, 0.06),
xlab=expression(italic(t)*"/s"), 
ylab=expression(italic(s)*"/V"))
points(coordinates$x, coordinates$y,, pch = 16, col = "red", main = "Exponential Fit", xlab = "x", ylab = "y")
#lines(x_interval, predict(exp_fit), col = "blue", lwd = 2)
lines(x_interval, predict(exp_fit, newdata = data.frame(x = x_interval)), col = "red")
lines(x_interval, -0.02-predict(exp_fit, newdata = data.frame(x = x_interval)), col = "red")

temp_e <- 7.592534e-06-0.003993673

print(temp_e)

#pdf

pdf.filename <- sprintf("plots/%s.pdf", filename)
dev.copy2pdf(file=pdf.filename, width=6, height=5)