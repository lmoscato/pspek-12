# Clear the workspace
rm(list = ls())

# Load necessary libraries
library(readr)

# Read background and sample data
background <- read.delim("~/Nico_Martim/NicoMartim Background _HR4D20551_16-54-16-280.txt", header = FALSE, stringsAsFactors=FALSE, skip = 15)
sample <- read.delim("~/Nico_Martim/NicoMartim Sample III neu_HR4D20551_17-14-57-086.txt", header = FALSE, stringsAsFactors=FALSE, skip = 15)

# Calculate wavenumbers and intensities
wavenum_auto <- 1/sample$V1*10^7
I_bg <- background$V2
I_samp <- sample$V2
I_corr <- I_samp - I_bg
max_bg_index <- which.max(I_bg)
wavenum <- -wavenum_auto + (1/532.639*10^7)

# Plot the corrected intensity against wavenumbers
plot(wavenum, I_corr, type = "l", xlim = c(4000, -1500), xaxs = "i", 
     xlab = expression("Wavenumber / cm"^(-1)), ylab = "Intensity", ylim = c(-50, 2000))

#############################################################################################
# 
#Extract peak around 2800
peak_wavenum <- wavenum[2800:2500]
peak_I <- I_corr[2800:2500]
b <- seq(wavenum[2800], wavenum[1550], length = 1800)
 
# Plot the p
plot(peak_wavenum, peak_I, type = "p")
#lines(b,  1000 * exp(-0.5 * ((b - 260)^2) / 7^2) +100 )
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) +50 ,
            start = list(height = 850, position = 2850, width = 50), trace = TRUE)

lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) +50 ,
             start = list(height = 850, position = 2850, width = 50), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))
 
# Generate sequences for plotting curves
a <-  seq(wavenum[2800], wavenum[2500], length = 1800)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")
 
# Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")


####################################################################
# 
#Extract peak around 1400
peak_wavenum <- wavenum[1690:1620]
peak_I <- I_corr[1690:1620]
b <- seq(wavenum[1690], wavenum[1620], length = 1800)
 
# Plot the p
plot(peak_wavenum, peak_I, type = "p")
#lines(b,  1000 * exp(-0.5 * ((b - 260)^2) / 7^2) +100 )
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) +200 ,
            start = list(height = 200, position = 1420, width = 20), trace = TRUE)
 
lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) +200 ,
             start = list(height = 200, position = 1420, width = 20), trace = TRUE)
print(summary(gauss))
 
print(summary(lorenz))
 
# Generate sequences for plotting curves
a <-  seq(wavenum[1690], wavenum[1620], length = 1800)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")

# Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")

# ###############################################################
####################################################################

#Extract peak around 1420
peak_wavenum <- wavenum[1590:1550]
peak_I <- I_corr[1590:1550]
b <- seq(wavenum[1590], wavenum[1550], length = 1800)

# Plot the p
plot(peak_wavenum, peak_I, type = "p")
#lines(b,  1000 * exp(-0.5 * ((b - 260)^2) / 7^2) +100 )
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) +200 ,
             start = list(height = 320, position = 1310, width = 20), trace = TRUE)

lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) +200 ,
              start = list(height = 320, position = 1310, width = 20), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))

# Generate sequences for plotting curves
a <-  seq(wavenum[1590], wavenum[1550], length = 1800)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")

# Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")

###############################################################

# 
# #Extract peak around 1360
peak_wavenum <- wavenum[1620:1583]
peak_I <- I_corr[1620:1583]
b <- seq(wavenum[1620], wavenum[1583], length = 1800)
 
# Plot the p
plot(peak_wavenum, peak_I, type = "p")
#lines(b,  1000 * exp(-0.5 * ((b - 260)^2) / 7^2) +100 )
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) +200 ,
            start = list(height = 350, position = 1360, width = 10), trace = TRUE)

lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) +200 ,
             start = list(height = 350, position = 1360, width = 10), trace = TRUE)
print(summary(gauss))
 
print(summary(lorenz))

# Generate sequences for plotting curves
a <-  seq(wavenum[1620], wavenum[1583], length = 1800)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")

# Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")
# 
#########################################################################


#Extract peak around 1000
peak_wavenum <- wavenum[1450:1315]
peak_I <- I_corr[1450:1315]
b <- seq(wavenum[1450], wavenum[1315], length = 1800)

# Plot the p
plot(peak_wavenum, peak_I, type = "p")
lines(b,  1000 * exp(-0.5 * ((b - 260)^2) / 7^2) +100 )
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) +150 ,
             start = list(height = 320, position = 1000, width = 20), trace = TRUE)

lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) +150 ,
              start = list(height = 320, position = 1000, width = 20), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))

# Generate sequences for plotting curves
a <-  seq(wavenum[1450], wavenum[1315], length = 1800)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")

# Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")
##########################################################################



#Extract peak around 800
peak_wavenum <- wavenum[1350:1215]
peak_I <- I_corr[1350:1215]
b <- seq(wavenum[1350], wavenum[1215], length = 1800)

# Plot the p
plot(peak_wavenum, peak_I, type = "p")
#lines(b,  1000 * exp(-0.5 * ((b - 260)^2) / 7^2) +100 )
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) +120 ,
             start = list(height = 100, position = 800, width = 60), trace = TRUE)

lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) +120 ,
              start = list(height = 100, position = 800, width = 60), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))

# Generate sequences for plotting curves
a <-  seq(wavenum[1350], wavenum[1215], length = 1800)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")

# Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")

#####################################################################
# 
 #Extract peak around 1650
peak_wavenum <- wavenum[1800:1750]
peak_I <- I_corr[1800:1750]
b <- seq(wavenum[1800], wavenum[1750], length = 1800)
# 
# Plot the p
plot(peak_wavenum, peak_I, type = "p",xlab=expression("Wavenumber / cm"^(-1)), ylab="Intensity", ylim=c(100,1000))
#lines(b,  1000 * exp(-0.5 * ((b - 260)^2) / 7^2) +100 )
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) +200 ,
            start = list(height = 850, position = 1640, width = 10), trace = TRUE)

lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) +150 ,
             start = list(height = 850, position = 1640, width = 10), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))
 
 # Generate sequences for plotting curves
a <-  seq(wavenum[1800], wavenum[1750], length = 1800)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")
 
# Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")

################################################

#Extract peak around 1140
peak_wavenum <- wavenum[1530:1490]
peak_I <- I_corr[1530:1490]
b <- seq(wavenum[1530], wavenum[1490], length = 1800)

# Plot the p 
plot(peak_wavenum, peak_I, type = "p")
#lines(b,  1000 * exp(-0.5 * ((b - 260)^2) / 7^2) +100 )
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) +160 ,
             start = list(height = 70, position = 1200, width = 20), trace = TRUE)

lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) +160 ,
              start = list(height = 70, position = 1200, width = 20), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))

# Generate sequences for plotting curves
a <-  seq(wavenum[1560], wavenum[1410], length = 1800)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")

# Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral") 
##########################################################################


#Extract peak around 400
peak_wavenum <- wavenum[1160:1050]
peak_I <- I_corr[1160:1050]
b <- seq(wavenum[1160], wavenum[1050], length = 1800)

# Plot the p
plot(peak_wavenum, peak_I, type = "p")
#lines(b,  1700 * exp(-0.5 * ((b - 680)^2) / 7^2) +100 )
# Fit Gaussian and Lorentzian peaks
gauss <- nls(peak_I ~ height * exp(-0.5 * ((peak_wavenum - position)^2) / width^2) +100 ,
             start = list(height = 60, position = 420, width = 7), trace = TRUE)
lorenz <- nls(peak_I ~ height * (width^2 / ((peak_wavenum - position)^2 + width^2)) +100 ,
              start = list(height = 60, position = 420, width = 7), trace = TRUE)
print(summary(gauss))

print(summary(lorenz))

# Generate sequences for plotting curves
a <-  seq(wavenum[1160], wavenum[1050], length = 1800)
curve_g <- predict(gauss, list(peak_wavenum = a), interval = "confidence")
curve_l <- predict(lorenz, list(peak_wavenum = a), interval = "confidence")

# Plot the curves
lines(a, curve_g, lwd = 2, col = "royalblue")
lines(a, curve_l, lwd = 2, col = "coral")
###########################################################################




# Plot the corrected intensity against wavenumbers
#plot(wavenum, I_corr, type = "l", ylim = c(0, max(I_corr)), xlim = c(4000, -1000), 
#xaxs = "i", xlab = expression("Wavenumber / cm"^(-1)), ylab = "Intensity")
# Plot the corrected intensity against wavenumbers
plot(wavenum, I_corr, type = "l", xlim = c(3200, 0), xaxs = "i", 
     xlab = expression("Wavenumber / cm"^(-1)), ylab = "Intensity", ylim = c(-50, 1300))

# Add text annotations
text(2883 , 1080, srt = 90, labels = expression("2883.06 cm"^(-1)), cex = 0.8, col = "royalblue")
text(1441.81, 560, srt = 90, labels = expression("1422.55 cm"^(-1)), cex = 0.8, col = "royalblue")
text(1290.96, 460, srt = 90, labels = expression("1303.96 cm"^(-1)), cex = 0.8, col = "royalblue")
text(1647.75, 1080, srt = 90, labels = expression("1647.75 cm"^(-1)), cex = 0.8, col = "royalblue")
#text(-100, 5200, srt = 90, labels = expression("Rayleigh scattering"), cex = 0.8, col = "black")
text(1360.12, 490, srt = 90, labels = expression("1360.12 cm"^(-1)), cex = 0.8, col = "royalblue")
text(988.80, 470, srt = 90, labels = expression("988.80 cm"^(-1)), cex = 0.8, col = "royalblue")
text(433.66, 325, srt = 90, labels = expression("433.66 cm"^(-1)), cex = 0.8, col = "royalblue")
text(794.06, 350, srt = 90, labels = expression("794.06 cm"^(-1)), cex = 0.8, col = "royalblue")
text(1192.44, 350, srt = 90, labels = expression("1192.45 cm"^(-1)), cex = 0.8, col = "royalblue")
