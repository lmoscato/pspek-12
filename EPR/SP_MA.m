% Modulation ampltiude

clear variables; close all;

[B_0, I_0, par_0] = eprload('data/SP_MA_0p05');
[B_1, I_1, par_1] = eprload('data/SP_MA_0p1');
[B_2, I_2, par_2] = eprload('data/SP_MA_0p2');
[B_3, I_3, par_3] = eprload('data/SP_MA_0p5');
[B_4, I_4, par_4] = eprload('data/SP_MA_1');
[B_5, I_5, par_5] = eprload('data/SP_MA_2');
[B_6, I_6, par_6] = eprload('data/SP_MA_5');
[B_7, I_7, par_7] = eprload('data/SP_MA_7p5');
[B_8, I_8, par_8] = eprload('data/SP_MA_10');
[B_9, I_9, par_9] = eprload('data/SP_MA_12p5');
[B_10, I_10, par_10] = eprload('data/SP_MA_15');


%%

B_0 = B_0 / 10; % Conversion to mT
B_1 = B_1 / 10; 
B_2 = B_2 / 10; 
B_3 = B_3 / 10; 
B_4 = B_4 / 10; 
B_5 = B_5 / 10; 
B_6 = B_6 / 10; 
B_7 = B_7 / 10; 
B_8 = B_8 / 10; 
B_9 = B_9 / 10; 
B_10 = B_10 / 10;

%%

hold on, box off;

plot(B_0, I_0);
plot(B_1, I_1);
plot(B_2, I_2);
plot(B_3, I_3);
plot(B_4, I_4);
plot(B_5, I_5);
plot(B_6, I_6);
plot(B_7, I_7);
plot(B_8, I_8);
plot(B_9, I_9);
plot(B_10, I_10);
hold off;

fontname('CMU Serif');
lg = legend('0.05', '0.1', '0.2', '0.5', '1', '2', '5', '7.5', '10', '12.5', '15'); % nice legend
fontsize(lg, 6, "points");
title(lg, 'Modulation amplitude [G]')
xlim([B_0(1) B_0(end)]);
xlabel('Magnetic Field [mT]');
ylabel('Intensity a.u.');

print('plots/SP_MA', '-dpng');

%%

%Calculate peak-to-peak distances

idx_max_0 =     find(I_0 == max(I_0));
idx_max_1 =     find(I_1 == max(I_1));
idx_max_2 =     find(I_2 == max(I_2));
idx_max_3 =     find(I_3 == max(I_3));
idx_max_4 =     find(I_4 == max(I_4));
idx_max_5 =     find(I_5 == max(I_5));
idx_max_6 =     find(I_6 == max(I_6));
idx_max_7 =     find(I_7 == max(I_7));
idx_max_8 =     find(I_8 == max(I_8));
idx_max_9 =     find(I_9 == max(I_9));
idx_max_10 =    find(I_10 == max(I_10));

idx_min_0 =     find(I_0 == min(I_0));
idx_min_1 =     find(I_1 == min(I_1));
idx_min_2 =     find(I_2 == min(I_2));
idx_min_3 =     find(I_3 == min(I_3));
idx_min_4 =     find(I_4 == min(I_4));
idx_min_5 =     find(I_5 == min(I_5));
idx_min_6 =     find(I_6 == min(I_6));
idx_min_7 =     find(I_7 == min(I_7));
idx_min_8 =     find(I_8 == min(I_8));
idx_min_9 =     find(I_9 == min(I_9));
idx_min_10 =    find(I_10 == min(I_10));

delta_0     = max(B_0(idx_min_0)    - B_0(idx_max_0));
delta_1     = max(B_1(idx_min_1)    - B_0(idx_max_1));
delta_2     = max(B_2(idx_min_2)    - B_0(idx_max_2));
delta_3     = max(B_3(idx_min_3)    - B_0(idx_max_3));
delta_4     = max(B_4(idx_min_4)    - B_0(idx_max_4));
delta_5     = max(B_5(idx_min_5)    - B_0(idx_max_5));
delta_6     = max(B_6(idx_min_6)    - B_0(idx_max_6));
delta_7     = max(B_7(idx_min_7)    - B_0(idx_max_7));
delta_8     = max(B_8(idx_min_8)    - B_0(idx_max_8));
delta_9     = max(B_9(idx_min_9)    - B_0(idx_max_9));
delta_10    = max(B_10(idx_min_10)  - B_10(idx_max_10));



mod_amplitudes = [0.05 0.1 0.2 0.5 1 2 5 7.5 10 12.5 15]; % mod amplitudes
deltas = cat(2, delta_0, delta_1, delta_2, delta_3, delta_4, delta_5, delta_6, delta_7, delta_8, delta_9, delta_10); % concatenate deltas

% Find fitting polynomial

x = linspace(0, 15); % linear space along mod amplitude axis
pol = polyfit(mod_amplitudes, deltas, 2);
y = polyval(pol, x);

figure;
hold on, box off;
plot(mod_amplitudes, deltas, 'o')
plot(x,y)
xlabel('Modulation Amplitude [G]');
ylabel('\DeltaB_{pp} [mT]');
fontname("CMU Serif")

print('plots/MA_fit', '-dpng');