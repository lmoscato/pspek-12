#WAV-audiofile lesen
rm(list=ls())
library(tuneR)
library(httpgd)
cur_dir <- getwd()

if (!grepl("/APE", cur_dir, ignore.case = TRUE)) {
    setwd("./APE")
}

source("APECPVutils.R")

par(mfrow=c(2,2))

#x(t)###################


filename1 <- "fastsweep"
WAV.filename1 <- sprintf("audio/%s.wav", filename1)

WAV <- read.WAV(WAV.filename1)

sr <- WAV$Rate # [Hz]
dt <- 1 / sr # dwell time [s]

tx <- WAV$Time
x <- WAV$Signal

tx <-tx - trigger(tx,x, 0.1)
idx<- which(tx>-0.005 & tx< 0.1) #relevant data
tx<- tx[idx]
x<- x[idx]


#h(t)###########################################

filename <- "deltapuls_rec"
WAV.filename <- sprintf("recordings/%s.wav", filename)

WAV <- read.WAV(WAV.filename)

th <- WAV$Time
h <- WAV$Signal

th <-th - trigger(th,h, 0.1)
idx<- which(th>-0.005 & th< 0.1) #relevant data
th<- th[idx]
h<- h[idx]



#y(t)###########################################

filename2 <- "fastsweep_rec"
WAV.filename2 <- sprintf("recordings/%s.wav", filename2)

WAV <- read.WAV(WAV.filename2)

ty <- WAV$Time
y <- WAV$Signal
y<- y/max(abs(y)) #normierung

ty <-ty - trigger(ty,y, 0.1)
idx<- which(ty>-0.005 & ty< 0.2) #relevant data
ty<- ty[idx]
y<- y[idx]



######

# windows() per aprire finestra

plot(th,h, type = "l", xlim=c(),
xlab=expression(italic(t)*"/s"), 
ylab=expression(italic(h)*"/V"),
main="impuls response h(t)")

plot(tx,x, type = "l", xlim=c(),
xlab=expression(italic(t)*"/s"), 
ylab=expression(italic(x)*"/V"),
main=" simulation x(t)")

plot(ty,y, type = "l", xlim=c(),
xlab=expression(italic(t)*"/s"), 
ylab=expression(italic(y)*"/V"),
main=" y(t)")




#antwort berechnene mit faltung integral y =x * h

yc <- myconv(x,h)
yc <- yc/max(abs(yc))
t <- seq(from=0, by=dt, length.out= length(yc))
t<- t-trigger(t, yc, 0.1)
idx<- which(t>-0.005 & t< 0.2) #relevant data
tyc<- t[idx]
yc<- yc[idx]


plot(tyc,yc, type = "l", xlim=c(),
xlab=expression(italic(t)*"/s"), 
ylab=expression(italic(y)*"/V"),
main=" y(t) calculated")
#lines(ty,y, col="red")



par(mfrow=c(1,1))
plot(tyc,yc, type = "l", xlim=c(),
xlab=expression(italic(t)*"/s"), 
ylab=expression(italic(y)*"/V"),
main=" y(t) calculated")
lines(ty,y, col="red")



par(mfrow=c(1,1))
plot(tyc,yc, type = "l", xlim=c(0.1, 0.12),
xlab=expression(italic(t)*"/s"), 
ylab=expression(italic(y)*"/V"),
main=" y(t) calculated")
lines(ty,y, col="red")



#pdf

# pdf.filename <- sprintf("plots/%s.pdf", filename)
# dev.copy2pdf(file=pdf.filename, width=6, height=5)